#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#include <chrono>
#include <glob.h>
#include <TLorentzVector.h>


int Num_of_Events_in_an_input = 3000;
int Number_of_inputs = 2;

//using namespace TMVA;
using namespace std;
using namespace chrono;

//bool sortcol( const vector<float>& v1,const vector<float>& v2 ) {
//    return v1[0] > v2[0];
//}



vector <string> glob(const char *pattern) {
    glob_t g;
    glob(pattern, GLOB_TILDE, nullptr, &g); // one should ensure glob returns 0!
    vector <string> filelist;
    filelist.reserve(g.gl_pathc);
    for (size_t i = 0; i < g.gl_pathc; ++i) {
        filelist.emplace_back(g.gl_pathv[i]);
    }
    globfree(&g);
    return filelist;
}



int CheckConsistance(vector<float> Vector_Even, vector<float> Vector_Odd){

    if( (Vector_Even[0] == Vector_Even[0]) && (Vector_Even[1] == Vector_Even[1]) &&(Vector_Even[2] == Vector_Even[2]) &&(Vector_Even[3] == Vector_Even[3]) &&(Vector_Even[4] == Vector_Even[4]) &&(Vector_Even[5] == Vector_Even[5]) &&(Vector_Even[6] == Vector_Even[6])  ){
        return 1;
    }
    else{
        return 0;
    }

}

void swapTLV(TLorentzVector &TLV_1, TLorentzVector &TLV_2){
    TLorentzVector TLV_temp = TLV_1;
    TLV_1 = TLV_2;
    TLV_2 = TLV_temp;
}

bool cmp1(const vector<float> &a, const vector<float> &b)
{
    return a[5] > b[5];
}


int Create_ATLAS_Root() {


    //Input_Tree_Even
    TChain *chain_Calculate_MC_Even = new TChain("Elz_Sample");
    for (const auto &filename : glob(
        "./Converting_root/Elz_a1rho_a_scalar.root")) {
        chain_Calculate_MC_Even->Add(filename.c_str());
    }
    Float_t tauspinner_0;
    vector<float> *Particle_Px_even = new vector<float>;
    vector<float> *Particle_Py_even = new vector<float>;
    vector<float> *Particle_Pz_even = new vector<float>;
    vector<float> *Particle_E_even = new vector<float>;
    vector<int> *Particle_Id_even = new vector<int>;
    chain_Calculate_MC_Even->SetBranchAddress("tauspinner", &tauspinner_0);
    chain_Calculate_MC_Even->SetBranchAddress("Particle_Px", &Particle_Px_even);
    chain_Calculate_MC_Even->SetBranchAddress("Particle_Py", &Particle_Py_even);
    chain_Calculate_MC_Even->SetBranchAddress("Particle_Pz", &Particle_Pz_even);
    chain_Calculate_MC_Even->SetBranchAddress("Particle_E", &Particle_E_even);
    chain_Calculate_MC_Even->SetBranchAddress("Particle_Id", &Particle_Id_even);






    //Input_Tree_Odd
    TChain *chain_Calculate_MC_Odd = new TChain("Elz_Sample");
    for (const auto &filename : glob(
        "./Converting_root/Elz_a1rho_a_pseudoscalar.root")) {
        chain_Calculate_MC_Odd->Add(filename.c_str());
    }
    Float_t tauspinner_90;
    vector<float> *Particle_Px_odd = new vector<float>;
    vector<float> *Particle_Py_odd = new vector<float>;
    vector<float> *Particle_Pz_odd = new vector<float>;
    vector<float> *Particle_E_odd = new vector<float>;
    vector<int> *Particle_Id_odd = new vector<int>;
    chain_Calculate_MC_Odd->SetBranchAddress("tauspinner", &tauspinner_90);
    chain_Calculate_MC_Odd->SetBranchAddress("Particle_Px", &Particle_Px_odd);
    chain_Calculate_MC_Odd->SetBranchAddress("Particle_Py", &Particle_Py_odd);
    chain_Calculate_MC_Odd->SetBranchAddress("Particle_Pz", &Particle_Pz_odd);
    chain_Calculate_MC_Odd->SetBranchAddress("Particle_E", &Particle_E_odd);
    chain_Calculate_MC_Odd->SetBranchAddress("Particle_Id", &Particle_Id_odd);



    //Output_Tree
    Double_t tauspinner_HCP_Theta_0;
    Double_t tauspinner_HCP_Theta_90;
    UInt_t tau_0_n_charged_tracks;
    UInt_t tau_1_n_charged_tracks;
    Float_t tau_0_track0_charge;
    Float_t tau_0_track1_charge;
    Float_t tau_0_track2_charge;
    TLorentzVector *tau_0_track0_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track1_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track2_p4 = new TLorentzVector;
    TLorentzVector *ditau_p4 = new TLorentzVector;
    TLorentzVector *tau_1_decay_charged_p4 = new TLorentzVector;
    TLorentzVector *tau_1_decay_neutral_p4 = new TLorentzVector;
    UInt_t tau_0_decay_mode;
    UInt_t tau_1_decay_mode;
    Float_t ditau_qxq;
    Float_t tau_0_q;
    Float_t tau_1_q;
    TLorentzVector *tau_0_neutrino_p4 = new TLorentzVector;
    TLorentzVector *tau_1_neutrino_p4 = new TLorentzVector;


    TFile *fN = new TFile("./ATLAS_type_output/ATLAS_Sample_Elz_a1rho_a_fix.root", "RECREATE");
    TTree *Converted_Tree = new TTree("Converted_Tree", "Converted_Tree");
    Converted_Tree->SetDirectory(fN);

    Converted_Tree->Branch("tauspinner_HCP_Theta_0", &tauspinner_HCP_Theta_0);
    Converted_Tree->Branch("tauspinner_HCP_Theta_90", &tauspinner_HCP_Theta_90);
    Converted_Tree->Branch("tau_0_q", &tau_0_q);
    Converted_Tree->Branch("tau_1_q", &tau_1_q);
    Converted_Tree->Branch("tau_0_track0_p4", &tau_0_track0_p4);
    Converted_Tree->Branch("tau_0_track1_p4", &tau_0_track1_p4);
    Converted_Tree->Branch("tau_0_track2_p4", &tau_0_track2_p4);
    Converted_Tree->Branch("ditau_p4", &ditau_p4);
    Converted_Tree->Branch("tau_1_decay_charged_p4", &tau_1_decay_charged_p4);
    Converted_Tree->Branch("tau_1_decay_neutral_p4", &tau_1_decay_neutral_p4);
    Converted_Tree->Branch("tau_0_track0_charge", &tau_0_track0_charge);
    Converted_Tree->Branch("tau_0_track1_charge", &tau_0_track1_charge);
    Converted_Tree->Branch("tau_0_track2_charge", &tau_0_track2_charge);
    Converted_Tree->Branch("tau_0_neutrino_p4", &tau_0_neutrino_p4);
    Converted_Tree->Branch("tau_1_neutrino_p4", &tau_1_neutrino_p4);

    Converted_Tree->Branch("tau_0_decay_mode", &tau_0_decay_mode);
    Converted_Tree->Branch("tau_1_decay_mode", &tau_1_decay_mode);
    Converted_Tree->Branch("ditau_qxq", &ditau_qxq);
    Converted_Tree->Branch("tau_0_n_charged_tracks", &tau_0_n_charged_tracks);
    Converted_Tree->Branch("tau_1_n_charged_tracks", &tau_1_n_charged_tracks);



    Int_t nentries_Even = (Int_t) chain_Calculate_MC_Even->GetEntries();
    Int_t nentries_Odd = (Int_t) chain_Calculate_MC_Odd->GetEntries();
    if(nentries_Even != nentries_Odd){
        cout<<"Error: Sample Not Match"<<endl;
        return 0;
    }


    for (Int_t i = 0; i < nentries_Even; i++) {
        chain_Calculate_MC_Even->GetEntry(i);
        chain_Calculate_MC_Odd->GetEntry(i);

        if(CheckConsistance(*Particle_E_even,*Particle_E_odd) == 0 ){
            cout<<"Error: Sample Energy Not Match"<<endl;
            return 0;
        }

        TLorentzVector tau_0_track_ori_0;
        TLorentzVector tau_0_track_ori_1;
        TLorentzVector tau_0_track_ori_2;
        TLorentzVector tau_0_neutrino;

        TLorentzVector tau_1_charged_pion;
        TLorentzVector tau_1_neutral_pion;
        TLorentzVector tau_1_neutrino;


//        cout<<(*Particle_Px_odd)[2]<<endl;
//        cout<<(*Particle_Py_odd)[2]<<endl;
//        cout<<(*Particle_Pz_odd)[2]<<endl;
//        cout<<(*Particle_E_odd)[2]<<endl;
//        cout<<(*Particle_Id_odd)[2]<<endl;

        tauspinner_HCP_Theta_0 = tauspinner_0;
        tauspinner_HCP_Theta_90 = tauspinner_90;


        if((*Particle_Id_odd)[0] == 16){
            tau_0_q = -1;
            tau_1_q = 1;

        }
        else if((*Particle_Id_odd)[0] == -16){
            tau_0_q = 1;
            tau_1_q = -1;
        }
        else{
            cout<<"Error: Wrong ID: neutrino = "<<(*Particle_Id_odd)[0]<<endl;
        }
        tau_0_neutrino_p4->SetPxPyPzE((*Particle_Px_odd)[0],(*Particle_Py_odd)[0],(*Particle_Pz_odd)[0],(*Particle_E_odd)[0]);
        tau_1_neutrino_p4->SetPxPyPzE((*Particle_Px_odd)[4],(*Particle_Py_odd)[4],(*Particle_Pz_odd)[4],(*Particle_E_odd)[4]);

        if(((*Particle_Id_odd)[6] == 111) && ((*Particle_Id_odd)[5] == 211 || (*Particle_Id_odd)[5] == -211) &&((*Particle_Id_odd)[1] == 211 || (*Particle_Id_odd)[1] == -211) &&((*Particle_Id_odd)[2] == 211 || (*Particle_Id_odd)[2] == -211)  &&((*Particle_Id_odd)[3] == 211 || (*Particle_Id_odd)[3] == -211)  ){



            tau_0_track_ori_0.SetPxPyPzE((*Particle_Px_odd)[1],(*Particle_Py_odd)[1],(*Particle_Pz_odd)[1],(*Particle_E_odd)[1]);
            tau_0_track_ori_1.SetPxPyPzE((*Particle_Px_odd)[2],(*Particle_Py_odd)[2],(*Particle_Pz_odd)[2],(*Particle_E_odd)[2]);
            tau_0_track_ori_2.SetPxPyPzE((*Particle_Px_odd)[3],(*Particle_Py_odd)[3],(*Particle_Pz_odd)[3],(*Particle_E_odd)[3]);

            vector<vector<float>> tau_0_tracks_info;
            vector<float> tau_0_t0_info;
            tau_0_t0_info.push_back((*Particle_Px_odd)[1]);
            tau_0_t0_info.push_back((*Particle_Py_odd)[1]);
            tau_0_t0_info.push_back((*Particle_Pz_odd)[1]);
            tau_0_t0_info.push_back((*Particle_E_odd)[1]);
            tau_0_t0_info.push_back(float((*Particle_Id_odd)[1]));
            tau_0_t0_info.push_back(float(tau_0_track_ori_0.Pt()));

            vector<float> tau_0_t1_info;
            tau_0_t1_info.push_back((*Particle_Px_odd)[2]);
            tau_0_t1_info.push_back((*Particle_Py_odd)[2]);
            tau_0_t1_info.push_back((*Particle_Pz_odd)[2]);
            tau_0_t1_info.push_back((*Particle_E_odd)[2]);
            tau_0_t1_info.push_back(float((*Particle_Id_odd)[2]));
            tau_0_t1_info.push_back(float(tau_0_track_ori_1.Pt()));

            vector<float> tau_0_t2_info;
            tau_0_t2_info.push_back((*Particle_Px_odd)[3]);
            tau_0_t2_info.push_back((*Particle_Py_odd)[3]);
            tau_0_t2_info.push_back((*Particle_Pz_odd)[3]);
            tau_0_t2_info.push_back((*Particle_E_odd)[3]);
            tau_0_t2_info.push_back(float((*Particle_Id_odd)[3]));
            tau_0_t2_info.push_back(float(tau_0_track_ori_2.Pt()));

            tau_0_tracks_info.push_back(tau_0_t0_info);
            tau_0_tracks_info.push_back(tau_0_t1_info);
            tau_0_tracks_info.push_back(tau_0_t2_info);

            sort(tau_0_tracks_info.begin(), tau_0_tracks_info.end(), cmp1);

            if(tau_0_tracks_info[0][5] >= tau_0_tracks_info[1][5]  && tau_0_tracks_info[1][5] >= tau_0_tracks_info[2][5]  ){
            }
            else{
                cout<<"Wrong Swap"<<endl;
                cout<<tau_0_tracks_info[0][5]<<endl;
                cout<<tau_0_tracks_info[1][5]<<endl;
                cout<<tau_0_tracks_info[2][5]<<endl;
                cout<<"=========="<<endl;

            }

            tau_0_track0_p4->SetPxPyPzE(tau_0_tracks_info[0][0],tau_0_tracks_info[0][1],tau_0_tracks_info[0][2],tau_0_tracks_info[0][3]);
            tau_0_track1_p4->SetPxPyPzE(tau_0_tracks_info[1][0],tau_0_tracks_info[1][1],tau_0_tracks_info[1][2],tau_0_tracks_info[1][3]);
            tau_0_track2_p4->SetPxPyPzE(tau_0_tracks_info[2][0],tau_0_tracks_info[2][1],tau_0_tracks_info[2][2],tau_0_tracks_info[2][3]);

            tau_0_track0_charge = float (tau_0_tracks_info[0][4]/211);
            tau_0_track1_charge = float (tau_0_tracks_info[1][4]/211);
            tau_0_track2_charge = float (tau_0_tracks_info[2][4]/211);

            tau_1_decay_charged_p4->SetPxPyPzE((*Particle_Px_odd)[5],(*Particle_Py_odd)[5],(*Particle_Pz_odd)[5],(*Particle_E_odd)[5]);
            tau_1_decay_neutral_p4->SetPxPyPzE((*Particle_Px_odd)[6],(*Particle_Py_odd)[6],(*Particle_Pz_odd)[6],(*Particle_E_odd)[6]);


            *ditau_p4 = *tau_0_track0_p4 + *tau_0_track1_p4 + *tau_0_track2_p4 + *tau_1_decay_charged_p4 + *tau_1_decay_neutral_p4;

            tau_0_decay_mode = int(3);
            tau_1_decay_mode = int(1);
            ditau_qxq = float(-1);
            tau_0_n_charged_tracks = int(3);
            tau_1_n_charged_tracks = int(1);



        }
        else{
            cout<<"Error: Pion_Error"<<endl;
        }







        Converted_Tree->Fill();
//        cout<<"==========="<<endl;

    }

    fN->Write();

//
    return 0;

}


void Convert_to_ATLAS() {


//    Make_Plots_for_d0();
//    Make_Plots_for_z0();
//    Make_Plots_for_distance();
//    Make_Plots_for_z0_diff();
//    Make_Plots_for_1p3p_rho_ip();
//    Check_Reco_Decay_vtx_exist();

    Create_ATLAS_Root();


    return;
}

