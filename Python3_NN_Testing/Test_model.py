import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import json

from torch.utils.data import Dataset
from torch.utils.data import DataLoader

EPOCHS_TO_TRAIN = 200000

Num_of_events_in_single_input = 59
Num_of_bins = 7
Batch_size_of_NN = 20

Num_of_Events_in_an_input = 3000
Number_of_inputs = 800


class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        # self.fc1 = nn.Linear(6, 1, True)
        # nn.init.ones_(self.fc1.weight)
        # self.fc1.bias.data.fill_(0.31)
        self.fc1 = nn.Linear(Num_of_events_in_single_input, 88, True)
        self.fc2 = nn.Linear(88, 12, True)
        self.dropout = nn.Dropout(p=0.1)
        # self.fc3 = nn.Linear(40, 12, True)
        self.fc6 = nn.Linear(12, 2, True)

    def forward(self, x):
        # Shrink_const = torch.tensor(0.159155)
        # Shrink_const.requires_grad_(True)

        x = F.tanh(self.fc1(x))
        x = F.tanh(self.fc2(x))
        x = self.dropout(x)
        # x = F.tanh(self.fc3(x))
        x = F.sigmoid(self.fc6(x))
        # x = Shrink_const * x
        # print(x.size())
        x_x = torch.index_select(x, dim=2, index=(torch.tensor([0])).cuda())
        # print(x_x.size())
        x_y = torch.index_select(x, dim=2, index=(torch.tensor([1])).cuda())
        Tan_value = x_y / (x_x + torch.tensor(0.000000001).cuda())
        angle = torch.atan(Tan_value)
        constant_pi = torch.tensor(3.14159265).cuda()
        const_sigmoid_scale = torch.tensor(100.0).cuda()
        output = angle + F.sigmoid(-x_x * const_sigmoid_scale) * constant_pi + F.sigmoid(
            -x_y * const_sigmoid_scale) * F.sigmoid(x_x * const_sigmoid_scale) * constant_pi * 2
        Shrink_const = torch.tensor(0.159155).cuda()
        Shrink_const.requires_grad_(True)
        output = Shrink_const * output
        # print(output)
        return output


# net = Net()
# net = torch.load('./CUDA_dropout_Elz_042802/Epoch_9350_Phistar_model.pkl')

#
# net.cuda()
# net.train()


class My_1p1p_Dataset(Dataset):

    def __init__(self):
        with open('./../2021-12-12/NN_Samples/Test_NN_Inputs_Elz_S_with_B_121201.json',
                  'r') as f_input:
            data_input = json.load(f_input)
        with open('./../2021-12-12/NN_Samples/Test_NN_Labels_Elz_S_with_B_121201.json',
                  'r') as f_result:
            data_result = json.load(f_result)
        print(len(data_input))
        self.reshaped_input_data = np.reshape(data_input,
                                              (-1, Num_of_Events_in_an_input, Num_of_events_in_single_input))
        self.reshaped_result_data = np.reshape(data_result, (-1, Num_of_Events_in_an_input, 2))
        self.Input_Data = torch.from_numpy(self.reshaped_input_data)
        self.len = self.Input_Data.shape[0]

    def __getitem__(self, index):
        self.label_of_data = torch.from_numpy(self.reshaped_result_data[index]).float()
        self.input_of_data_without_label = torch.from_numpy(self.reshaped_input_data[index]).float()

        return self.input_of_data_without_label, self.label_of_data

    def __len__(self):
        return self.len


criterion = nn.MSELoss()


def Cal_loss(predicted_values, label_values):
    Test_L1 = nn.Linear(1, 1, True)
    nn.init.ones_(Test_L1.weight)
    Test_L1.bias.data.fill_(0)
    # Test_Out = F.sigmoid(torch.tensor(10)*F.selu(Test_L1(predicted_values-torch.tensor(3))))
    # Test_Out = torch.log(Test_L1(predicted_values))
    # Test_Out = torch.log(predicted_values)
    # New_test = torch.log(predicted_values)
    # Test_Out = F.sigmoid(New_test) * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(0.00001)
    # print("input = ", predicted_values)

    # Test_Out.requires_grad_(True)
    # Test_Out.retain_grad()
    # Test_Out = F.sigmoid(F.selu(torch.tensor(10)*(predicted_values-torch.tensor(0.5))))
    # Test_input = torch.tensor(0.49)
    # Test_Function =F.sigmoid(torch.tensor(100)*F.selu(torch.tensor(0.1)))
    # print("Test_function = ", Test_Function)
    # print("output = ", torch.index_select(Test_Out, dim=1, index=(torch.tensor([0])).cuda()))

    # Bin_0_LB = torch.ge(predicted_values, 0.0).float()
    # Bin_0_UB = torch.lt(predicted_values, 0.142857).float()
    # Bin_0 = torch.where(Bin_0_LB > 0, Bin_0_UB, Bin_0_LB)
    Bin_0 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.0)) * (torch.tensor(0.142857) - predicted_values)))

    # print(Bin_0.device)
    Bin_0_Even = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    # print(Bin_0_Even.device)
    Bin_0_Odd = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_1_LB = torch.ge(predicted_values, 0.142857).float()
    # Bin_1_UB = torch.lt(predicted_values, 0.285714).float()
    Bin_1 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.142857)) * (torch.tensor(0.285714) - predicted_values)))
    Bin_1_Even = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_1_Odd = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_2_LB = torch.ge(predicted_values, 0.285714).float()
    # Bin_2_UB = torch.lt(predicted_values, 0.428571).float()
    Bin_2 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.285714)) * (torch.tensor(0.428571) - predicted_values)))
    Bin_2_Even = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_2_Odd = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_3_LB = torch.ge(predicted_values, 0.428571).float()
    # Bin_3_UB = torch.lt(predicted_values, 0.571428).float()
    Bin_3 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.428571)) * (torch.tensor(0.571428) - predicted_values)))
    Bin_3_Even = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_3_Odd = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_4_LB = torch.ge(predicted_values, 0.571428).float()
    # Bin_4_UB = torch.lt(predicted_values, 0.714285).float()
    Bin_4 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.571428)) * (torch.tensor(0.714285) - predicted_values)))
    Bin_4_Even = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_4_Odd = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_5_LB = torch.ge(predicted_values, 0.714285).float()
    # Bin_5_UB = torch.lt(predicted_values, 0.857142).float()
    Bin_5 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.714285)) * (torch.tensor(0.857142) - predicted_values)))
    Bin_5_Even = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_5_Odd = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_6_LB = torch.ge(predicted_values, 0.857142).float()
    # Bin_6_UB = torch.lt(predicted_values, 1).float()
    Bin_6 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.857142)) * (torch.tensor(1.0) - predicted_values)))
    Bin_6_Even = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_6_Odd = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bins_Even_Sum = Bin_0_Even.sum(1) + Bin_1_Even.sum(1) + Bin_2_Even.sum(1) + Bin_3_Even.sum(1) + Bin_4_Even.sum(
        1) + Bin_5_Even.sum(1) + Bin_6_Even.sum(1)

    Bins_Odd_Sum = Bin_0_Odd.sum(1) + Bin_1_Odd.sum(1) + Bin_2_Odd.sum(1) + Bin_3_Odd.sum(1) + Bin_4_Odd.sum(
        1) + Bin_5_Odd.sum(1) + Bin_6_Odd.sum(1)

    Even_Base = -(((Bin_0_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_0_Even.sum(1) / Bins_Even_Sum)) + (
            (Bin_1_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_1_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_2_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_2_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_3_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_3_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_4_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_4_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_5_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_5_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_6_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_6_Even.sum(1) / Bins_Even_Sum)))

    Odd_NLL = -(((Bin_0_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_0_Even.sum(1) / Bins_Even_Sum)) + (
            (Bin_1_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_1_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_2_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_2_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_3_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_3_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_4_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_4_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_5_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_5_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_6_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_6_Even.sum(1) / Bins_Even_Sum)))
    loss_value = -(Odd_NLL - Even_Base)

    Bins_Even_Sum.requires_grad_(True)
    Even_Base.requires_grad_(True)
    Odd_NLL.requires_grad_(True)
    loss_value.requires_grad_(True)

    Mean_Of_NLL_Value = torch.mean(loss_value)

    return Mean_Of_NLL_Value


def Cal_fixed_dnll_loss(predicted_values, label_values):
    Test_L1 = nn.Linear(1, 1, True)
    nn.init.ones_(Test_L1.weight)
    Test_L1.bias.data.fill_(0)
    # Test_Out = F.sigmoid(torch.tensor(10)*F.selu(Test_L1(predicted_values-torch.tensor(3))))
    # Test_Out = torch.log(Test_L1(predicted_values))
    # Test_Out = torch.log(predicted_values)
    # New_test = torch.log(predicted_values)
    # Test_Out = F.sigmoid(New_test) * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(0.00001)
    # print("input = ", predicted_values)

    # Test_Out.requires_grad_(True)
    # Test_Out.retain_grad()
    # Test_Out = F.sigmoid(F.selu(torch.tensor(10)*(predicted_values-torch.tensor(0.5))))
    # Test_input = torch.tensor(0.49)
    # Test_Function =F.sigmoid(torch.tensor(100)*F.selu(torch.tensor(0.1)))
    # print("Test_function = ", Test_Function)
    # print("output = ", torch.index_select(Test_Out, dim=1, index=(torch.tensor([0])).cuda()))

    # Bin_0_LB = torch.ge(predicted_values, 0.0).float()
    # Bin_0_UB = torch.lt(predicted_values, 0.142857).float()
    # Bin_0 = torch.where(Bin_0_LB > 0, Bin_0_UB, Bin_0_LB)
    Bin_0 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.0)) * (torch.tensor(0.142857) - predicted_values)))

    # print(Bin_0.device)
    Bin_0_Even = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    # print(Bin_0_Even.device)
    Bin_0_Odd = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_1_LB = torch.ge(predicted_values, 0.142857).float()
    # Bin_1_UB = torch.lt(predicted_values, 0.285714).float()
    Bin_1 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.142857)) * (torch.tensor(0.285714) - predicted_values)))
    Bin_1_Even = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_1_Odd = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_2_LB = torch.ge(predicted_values, 0.285714).float()
    # Bin_2_UB = torch.lt(predicted_values, 0.428571).float()
    Bin_2 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.285714)) * (torch.tensor(0.428571) - predicted_values)))
    Bin_2_Even = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_2_Odd = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_3_LB = torch.ge(predicted_values, 0.428571).float()
    # Bin_3_UB = torch.lt(predicted_values, 0.571428).float()
    Bin_3 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.428571)) * (torch.tensor(0.571428) - predicted_values)))
    Bin_3_Even = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_3_Odd = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_4_LB = torch.ge(predicted_values, 0.571428).float()
    # Bin_4_UB = torch.lt(predicted_values, 0.714285).float()
    Bin_4 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.571428)) * (torch.tensor(0.714285) - predicted_values)))
    Bin_4_Even = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_4_Odd = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_5_LB = torch.ge(predicted_values, 0.714285).float()
    # Bin_5_UB = torch.lt(predicted_values, 0.857142).float()
    Bin_5 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.714285)) * (torch.tensor(0.857142) - predicted_values)))
    Bin_5_Even = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_5_Odd = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_6_LB = torch.ge(predicted_values, 0.857142).float()
    # Bin_6_UB = torch.lt(predicted_values, 1).float()
    Bin_6 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.857142)) * (torch.tensor(1.0) - predicted_values)))
    Bin_6_Even = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_6_Odd = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    stirling_constant = torch.tensor(2.50662827)
    log_stirling_constant = torch.log(stirling_constant)

    Bin_0_Even_sum = Bin_0_Even.sum(1)
    Bin_0_Odd_sum = Bin_0_Odd.sum(1)

    Bin_1_Even_sum = Bin_1_Even.sum(1)
    Bin_1_Odd_sum = Bin_1_Odd.sum(1)

    Bin_2_Even_sum = Bin_2_Even.sum(1)
    Bin_2_Odd_sum = Bin_2_Odd.sum(1)

    Bin_3_Even_sum = Bin_3_Even.sum(1)
    Bin_3_Odd_sum = Bin_3_Odd.sum(1)

    Bin_4_Even_sum = Bin_4_Even.sum(1)
    Bin_4_Odd_sum = Bin_4_Odd.sum(1)

    Bin_5_Even_sum = Bin_5_Even.sum(1)
    Bin_5_Odd_sum = Bin_5_Odd.sum(1)

    Bin_6_Even_sum = Bin_6_Even.sum(1)
    Bin_6_Odd_sum = Bin_6_Odd.sum(1)

    LogLikelihood_Odd_Even_Bin_0 = (Bin_0_Odd_sum - Bin_0_Even_sum) + Bin_0_Odd_sum * torch.log(
        Bin_0_Even_sum) - log_stirling_constant - (
                                           Bin_0_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_0_Odd_sum)

    LogLikelihood_Odd_Even_Bin_1 = (Bin_1_Odd_sum - Bin_1_Even_sum) + Bin_1_Odd_sum * torch.log(
        Bin_1_Even_sum) - log_stirling_constant - (
                                           Bin_1_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_1_Odd_sum)

    LogLikelihood_Odd_Even_Bin_2 = (Bin_2_Odd_sum - Bin_2_Even_sum) + Bin_2_Odd_sum * torch.log(
        Bin_2_Even_sum) - log_stirling_constant - (
                                           Bin_2_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_2_Odd_sum)

    LogLikelihood_Odd_Even_Bin_3 = (Bin_3_Odd_sum - Bin_3_Even_sum) + Bin_3_Odd_sum * torch.log(
        Bin_3_Even_sum) - log_stirling_constant - (
                                           Bin_3_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_3_Odd_sum)

    LogLikelihood_Odd_Even_Bin_4 = (Bin_4_Odd_sum - Bin_4_Even_sum) + Bin_4_Odd_sum * torch.log(
        Bin_4_Even_sum) - log_stirling_constant - (
                                           Bin_4_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_4_Odd_sum)

    LogLikelihood_Odd_Even_Bin_5 = (Bin_5_Odd_sum - Bin_5_Even_sum) + Bin_5_Odd_sum * torch.log(
        Bin_5_Even_sum) - log_stirling_constant - (
                                           Bin_5_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_5_Odd_sum)

    LogLikelihood_Odd_Even_Bin_6 = (Bin_6_Odd_sum - Bin_6_Even_sum) + Bin_6_Odd_sum * torch.log(
        Bin_6_Even_sum) - log_stirling_constant - (
                                           Bin_6_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_6_Odd_sum)

    LogLikelihood_Even_Even_Bin_0 = (Bin_0_Even_sum - Bin_0_Even_sum) + Bin_0_Even_sum * torch.log(
        Bin_0_Even_sum) - log_stirling_constant - (
                                            Bin_0_Even_sum + torch.tensor(0.5)) * torch.log(Bin_0_Even_sum)

    LogLikelihood_Even_Even_Bin_1 = (Bin_1_Even_sum - Bin_1_Even_sum) + Bin_1_Even_sum * torch.log(
        Bin_1_Even_sum) - log_stirling_constant - (
                                            Bin_1_Even_sum + torch.tensor(0.5)) * torch.log(Bin_1_Even_sum)

    LogLikelihood_Even_Even_Bin_2 = (Bin_2_Even_sum - Bin_2_Even_sum) + Bin_2_Even_sum * torch.log(
        Bin_2_Even_sum) - log_stirling_constant - (
                                            Bin_2_Even_sum + torch.tensor(0.5)) * torch.log(Bin_2_Even_sum)

    LogLikelihood_Even_Even_Bin_3 = (Bin_3_Even_sum - Bin_3_Even_sum) + Bin_3_Even_sum * torch.log(
        Bin_3_Even_sum) - log_stirling_constant - (
                                            Bin_3_Even_sum + torch.tensor(0.5)) * torch.log(Bin_3_Even_sum)

    LogLikelihood_Even_Even_Bin_4 = (Bin_4_Even_sum - Bin_4_Even_sum) + Bin_4_Even_sum * torch.log(
        Bin_4_Even_sum) - log_stirling_constant - (
                                            Bin_4_Even_sum + torch.tensor(0.5)) * torch.log(Bin_4_Even_sum)

    LogLikelihood_Even_Even_Bin_5 = (Bin_5_Even_sum - Bin_5_Even_sum) + Bin_5_Even_sum * torch.log(
        Bin_5_Even_sum) - log_stirling_constant - (
                                            Bin_5_Even_sum + torch.tensor(0.5)) * torch.log(Bin_5_Even_sum)

    LogLikelihood_Even_Even_Bin_6 = (Bin_6_Even_sum - Bin_6_Even_sum) + Bin_6_Even_sum * torch.log(
        Bin_6_Even_sum) - log_stirling_constant - (
                                            Bin_6_Even_sum + torch.tensor(0.5)) * torch.log(Bin_6_Even_sum)

    Fixed_DLL = LogLikelihood_Odd_Even_Bin_0 + LogLikelihood_Odd_Even_Bin_1 + LogLikelihood_Odd_Even_Bin_2 + LogLikelihood_Odd_Even_Bin_3 + LogLikelihood_Odd_Even_Bin_4 + LogLikelihood_Odd_Even_Bin_5 + LogLikelihood_Odd_Even_Bin_6 - (
            LogLikelihood_Even_Even_Bin_0 + LogLikelihood_Even_Even_Bin_1 + LogLikelihood_Even_Even_Bin_2 + LogLikelihood_Even_Even_Bin_3 + LogLikelihood_Even_Even_Bin_4 + LogLikelihood_Even_Even_Bin_5 + LogLikelihood_Even_Even_Bin_6)
    Fixed_DNLL = - Fixed_DLL
    # print(predicted_values)

    Fixed_DNLL.requires_grad_(True)
    Mean_Of_NLL_Value = torch.mean(Fixed_DLL)

    return Mean_Of_NLL_Value


def Cal_fixed_dnll_loss_with_bin_lower_bound(predicted_values, label_values):
    Test_L1 = nn.Linear(1, 1, True)
    nn.init.ones_(Test_L1.weight)
    Test_L1.bias.data.fill_(0)
    # Test_Out = F.sigmoid(torch.tensor(10)*F.selu(Test_L1(predicted_values-torch.tensor(3))))
    # Test_Out = torch.log(Test_L1(predicted_values))
    # Test_Out = torch.log(predicted_values)
    # New_test = torch.log(predicted_values)
    # Test_Out = F.sigmoid(New_test) * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(0.00001)
    # print("input = ", predicted_values)

    # Test_Out.requires_grad_(True)
    # Test_Out.retain_grad()
    # Test_Out = F.sigmoid(F.selu(torch.tensor(10)*(predicted_values-torch.tensor(0.5))))
    # Test_input = torch.tensor(0.49)
    # Test_Function =F.sigmoid(torch.tensor(100)*F.selu(torch.tensor(0.1)))
    # print("Test_function = ", Test_Function)
    # print("output = ", torch.index_select(Test_Out, dim=1, index=(torch.tensor([0])).cuda()))

    # Bin_0_LB = torch.ge(predicted_values, 0.0).float()
    # Bin_0_UB = torch.lt(predicted_values, 0.142857).float()
    # Bin_0 = torch.where(Bin_0_LB > 0, Bin_0_UB, Bin_0_LB)
    Bin_0 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.0)) * (torch.tensor(0.142857) - predicted_values)))

    # print(Bin_0.device)
    Bin_0_Even = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    # print(Bin_0_Even.device)
    Bin_0_Odd = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_1_LB = torch.ge(predicted_values, 0.142857).float()
    # Bin_1_UB = torch.lt(predicted_values, 0.285714).float()
    Bin_1 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.142857)) * (torch.tensor(0.285714) - predicted_values)))
    Bin_1_Even = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_1_Odd = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_2_LB = torch.ge(predicted_values, 0.285714).float()
    # Bin_2_UB = torch.lt(predicted_values, 0.428571).float()
    Bin_2 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.285714)) * (torch.tensor(0.428571) - predicted_values)))
    Bin_2_Even = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_2_Odd = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_3_LB = torch.ge(predicted_values, 0.428571).float()
    # Bin_3_UB = torch.lt(predicted_values, 0.571428).float()
    Bin_3 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.428571)) * (torch.tensor(0.571428) - predicted_values)))
    Bin_3_Even = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_3_Odd = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_4_LB = torch.ge(predicted_values, 0.571428).float()
    # Bin_4_UB = torch.lt(predicted_values, 0.714285).float()
    Bin_4 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.571428)) * (torch.tensor(0.714285) - predicted_values)))
    Bin_4_Even = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_4_Odd = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_5_LB = torch.ge(predicted_values, 0.714285).float()
    # Bin_5_UB = torch.lt(predicted_values, 0.857142).float()
    Bin_5 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.714285)) * (torch.tensor(0.857142) - predicted_values)))
    Bin_5_Even = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_5_Odd = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_6_LB = torch.ge(predicted_values, 0.857142).float()
    # Bin_6_UB = torch.lt(predicted_values, 1).float()
    Bin_6 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.857142)) * (torch.tensor(1.0) - predicted_values)))
    Bin_6_Even = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_6_Odd = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    stirling_constant = torch.tensor(2.50662827)
    log_stirling_constant = torch.log(stirling_constant)

    Bin_0_Even_sum = Bin_0_Even.sum(1)
    Bin_0_Odd_sum = Bin_0_Odd.sum(1)

    Bin_1_Even_sum = Bin_1_Even.sum(1)
    Bin_1_Odd_sum = Bin_1_Odd.sum(1)

    Bin_2_Even_sum = Bin_2_Even.sum(1)
    Bin_2_Odd_sum = Bin_2_Odd.sum(1)

    Bin_3_Even_sum = Bin_3_Even.sum(1)
    Bin_3_Odd_sum = Bin_3_Odd.sum(1)

    Bin_4_Even_sum = Bin_4_Even.sum(1)
    Bin_4_Odd_sum = Bin_4_Odd.sum(1)

    Bin_5_Even_sum = Bin_5_Even.sum(1)
    Bin_5_Odd_sum = Bin_5_Odd.sum(1)

    Bin_6_Even_sum = Bin_6_Even.sum(1)
    Bin_6_Odd_sum = Bin_6_Odd.sum(1)

    LogLikelihood_Odd_Even_Bin_0 = (Bin_0_Odd_sum - Bin_0_Even_sum) + Bin_0_Odd_sum * torch.log(
        Bin_0_Even_sum) - log_stirling_constant - (
                                           Bin_0_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_0_Odd_sum)

    LogLikelihood_Odd_Even_Bin_1 = (Bin_1_Odd_sum - Bin_1_Even_sum) + Bin_1_Odd_sum * torch.log(
        Bin_1_Even_sum) - log_stirling_constant - (
                                           Bin_1_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_1_Odd_sum)

    LogLikelihood_Odd_Even_Bin_2 = (Bin_2_Odd_sum - Bin_2_Even_sum) + Bin_2_Odd_sum * torch.log(
        Bin_2_Even_sum) - log_stirling_constant - (
                                           Bin_2_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_2_Odd_sum)

    LogLikelihood_Odd_Even_Bin_3 = (Bin_3_Odd_sum - Bin_3_Even_sum) + Bin_3_Odd_sum * torch.log(
        Bin_3_Even_sum) - log_stirling_constant - (
                                           Bin_3_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_3_Odd_sum)

    LogLikelihood_Odd_Even_Bin_4 = (Bin_4_Odd_sum - Bin_4_Even_sum) + Bin_4_Odd_sum * torch.log(
        Bin_4_Even_sum) - log_stirling_constant - (
                                           Bin_4_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_4_Odd_sum)

    LogLikelihood_Odd_Even_Bin_5 = (Bin_5_Odd_sum - Bin_5_Even_sum) + Bin_5_Odd_sum * torch.log(
        Bin_5_Even_sum) - log_stirling_constant - (
                                           Bin_5_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_5_Odd_sum)

    LogLikelihood_Odd_Even_Bin_6 = (Bin_6_Odd_sum - Bin_6_Even_sum) + Bin_6_Odd_sum * torch.log(
        Bin_6_Even_sum) - log_stirling_constant - (
                                           Bin_6_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_6_Odd_sum)

    LogLikelihood_Even_Even_Bin_0 = (Bin_0_Even_sum - Bin_0_Even_sum) + Bin_0_Even_sum * torch.log(
        Bin_0_Even_sum) - log_stirling_constant - (
                                            Bin_0_Even_sum + torch.tensor(0.5)) * torch.log(Bin_0_Even_sum)

    LogLikelihood_Even_Even_Bin_1 = (Bin_1_Even_sum - Bin_1_Even_sum) + Bin_1_Even_sum * torch.log(
        Bin_1_Even_sum) - log_stirling_constant - (
                                            Bin_1_Even_sum + torch.tensor(0.5)) * torch.log(Bin_1_Even_sum)

    LogLikelihood_Even_Even_Bin_2 = (Bin_2_Even_sum - Bin_2_Even_sum) + Bin_2_Even_sum * torch.log(
        Bin_2_Even_sum) - log_stirling_constant - (
                                            Bin_2_Even_sum + torch.tensor(0.5)) * torch.log(Bin_2_Even_sum)

    LogLikelihood_Even_Even_Bin_3 = (Bin_3_Even_sum - Bin_3_Even_sum) + Bin_3_Even_sum * torch.log(
        Bin_3_Even_sum) - log_stirling_constant - (
                                            Bin_3_Even_sum + torch.tensor(0.5)) * torch.log(Bin_3_Even_sum)

    LogLikelihood_Even_Even_Bin_4 = (Bin_4_Even_sum - Bin_4_Even_sum) + Bin_4_Even_sum * torch.log(
        Bin_4_Even_sum) - log_stirling_constant - (
                                            Bin_4_Even_sum + torch.tensor(0.5)) * torch.log(Bin_4_Even_sum)

    LogLikelihood_Even_Even_Bin_5 = (Bin_5_Even_sum - Bin_5_Even_sum) + Bin_5_Even_sum * torch.log(
        Bin_5_Even_sum) - log_stirling_constant - (
                                            Bin_5_Even_sum + torch.tensor(0.5)) * torch.log(Bin_5_Even_sum)

    LogLikelihood_Even_Even_Bin_6 = (Bin_6_Even_sum - Bin_6_Even_sum) + Bin_6_Even_sum * torch.log(
        Bin_6_Even_sum) - log_stirling_constant - (
                                            Bin_6_Even_sum + torch.tensor(0.5)) * torch.log(Bin_6_Even_sum)

    Bin_num_lb = torch.tensor(2.0).cuda()
    Delta_LogLikelihood_Bin_0 = (LogLikelihood_Odd_Even_Bin_0 - LogLikelihood_Even_Even_Bin_0) * F.sigmoid(
        Bin_0_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_0_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_1 = (LogLikelihood_Odd_Even_Bin_1 - LogLikelihood_Even_Even_Bin_1) * F.sigmoid(
        Bin_1_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_1_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_2 = (LogLikelihood_Odd_Even_Bin_2 - LogLikelihood_Even_Even_Bin_2) * F.sigmoid(
        Bin_2_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_2_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_3 = (LogLikelihood_Odd_Even_Bin_3 - LogLikelihood_Even_Even_Bin_3) * F.sigmoid(
        Bin_3_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_3_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_4 = (LogLikelihood_Odd_Even_Bin_4 - LogLikelihood_Even_Even_Bin_4) * F.sigmoid(
        Bin_4_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_4_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_5 = (LogLikelihood_Odd_Even_Bin_5 - LogLikelihood_Even_Even_Bin_5) * F.sigmoid(
        Bin_5_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_5_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_6 = (LogLikelihood_Odd_Even_Bin_6 - LogLikelihood_Even_Even_Bin_6) * F.sigmoid(
        Bin_6_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_6_Even_sum - Bin_num_lb)

    Fixed_DLL = Delta_LogLikelihood_Bin_0 + Delta_LogLikelihood_Bin_1 + Delta_LogLikelihood_Bin_2 + Delta_LogLikelihood_Bin_3 + Delta_LogLikelihood_Bin_4 + Delta_LogLikelihood_Bin_5 + Delta_LogLikelihood_Bin_6

    # Fixed_DLL = LogLikelihood_Odd_Even_Bin_0 + LogLikelihood_Odd_Even_Bin_1 + LogLikelihood_Odd_Even_Bin_2 + LogLikelihood_Odd_Even_Bin_3 + LogLikelihood_Odd_Even_Bin_4 + LogLikelihood_Odd_Even_Bin_5 + LogLikelihood_Odd_Even_Bin_6 - (
    #         LogLikelihood_Even_Even_Bin_0 + LogLikelihood_Even_Even_Bin_1 + LogLikelihood_Even_Even_Bin_2 + LogLikelihood_Even_Even_Bin_3 + LogLikelihood_Even_Even_Bin_4 + LogLikelihood_Even_Even_Bin_5 + LogLikelihood_Even_Even_Bin_6)
    Fixed_DNLL = - Fixed_DLL
    # print(predicted_values)

    # Bin_lower_bound = torch.tensor(100.0).cuda()
    # Loss_Scale_when_below_lb = torch.tensor(100.0).cuda()

    # Loss_with_bin_lb = Fixed_DLL + Loss_Scale_when_below_lb * F.sigmoid(
    #     F.sigmoid(Bin_lower_bound - Bin_0_Even_sum) + F.sigmoid(Bin_lower_bound - Bin_1_Even_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_2_Even_sum) + F.sigmoid(Bin_lower_bound - Bin_3_Even_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_4_Even_sum) + F.sigmoid(Bin_lower_bound - Bin_5_Even_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_6_Even_sum) + F.sigmoid(Bin_lower_bound - Bin_0_Odd_sum) + F.sigmoid(Bin_lower_bound - Bin_1_Odd_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_2_Odd_sum) + F.sigmoid(Bin_lower_bound - Bin_3_Odd_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_4_Odd_sum) + F.sigmoid(Bin_lower_bound - Bin_5_Odd_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_6_Odd_sum) - torch.tensor(0.5).cuda())

    output = Fixed_DLL
    output.requires_grad_(True)
    Mean_Of_NLL_Value = torch.mean(output)

    return Mean_Of_NLL_Value


def Cal_loss_chisquare(predicted_values, label_values):
    Test_L1 = nn.Linear(1, 1, True)
    nn.init.ones_(Test_L1.weight)
    Test_L1.bias.data.fill_(0)
    # Test_Out = F.sigmoid(torch.tensor(10)*F.selu(Test_L1(predicted_values-torch.tensor(3))))
    # Test_Out = torch.log(Test_L1(predicted_values))
    # Test_Out = torch.log(predicted_values)
    # New_test = torch.log(predicted_values)
    # Test_Out = F.sigmoid(New_test) * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(0.00001)
    # print("input = ", predicted_values)

    # Test_Out.requires_grad_(True)
    # Test_Out.retain_grad()
    # Test_Out = F.sigmoid(F.selu(torch.tensor(10)*(predicted_values-torch.tensor(0.5))))
    # Test_input = torch.tensor(0.49)
    # Test_Function =F.sigmoid(torch.tensor(100)*F.selu(torch.tensor(0.1)))
    # print("Test_function = ", Test_Function)
    # print("output = ", torch.index_select(Test_Out, dim=1, index=(torch.tensor([0])).cuda()))

    # Bin_0_LB = torch.ge(predicted_values, 0.0).float()
    # Bin_0_UB = torch.lt(predicted_values, 0.142857).float()
    # Bin_0 = torch.where(Bin_0_LB > 0, Bin_0_UB, Bin_0_LB)
    Bin_0 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.0)) * (torch.tensor(0.142857) - predicted_values)))

    # print(Bin_0.device)
    Bin_0_Even = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    # print(Bin_0_Even.device)
    Bin_0_Odd = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_1_LB = torch.ge(predicted_values, 0.142857).float()
    # Bin_1_UB = torch.lt(predicted_values, 0.285714).float()
    Bin_1 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.142857)) * (torch.tensor(0.285714) - predicted_values)))
    Bin_1_Even = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_1_Odd = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_2_LB = torch.ge(predicted_values, 0.285714).float()
    # Bin_2_UB = torch.lt(predicted_values, 0.428571).float()
    Bin_2 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.285714)) * (torch.tensor(0.428571) - predicted_values)))
    Bin_2_Even = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_2_Odd = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_3_LB = torch.ge(predicted_values, 0.428571).float()
    # Bin_3_UB = torch.lt(predicted_values, 0.571428).float()
    Bin_3 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.428571)) * (torch.tensor(0.571428) - predicted_values)))
    Bin_3_Even = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_3_Odd = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_4_LB = torch.ge(predicted_values, 0.571428).float()
    # Bin_4_UB = torch.lt(predicted_values, 0.714285).float()
    Bin_4 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.571428)) * (torch.tensor(0.714285) - predicted_values)))
    Bin_4_Even = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_4_Odd = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_5_LB = torch.ge(predicted_values, 0.714285).float()
    # Bin_5_UB = torch.lt(predicted_values, 0.857142).float()
    Bin_5 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.714285)) * (torch.tensor(0.857142) - predicted_values)))
    Bin_5_Even = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_5_Odd = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_6_LB = torch.ge(predicted_values, 0.857142).float()
    # Bin_6_UB = torch.lt(predicted_values, 1).float()
    Bin_6 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.857142)) * (torch.tensor(1.0) - predicted_values)))
    Bin_6_Even = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_6_Odd = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bins_Even_Sum = Bin_0_Even.sum(1) + Bin_1_Even.sum(1) + Bin_2_Even.sum(1) + Bin_3_Even.sum(1) + Bin_4_Even.sum(
        1) + Bin_5_Even.sum(1) + Bin_6_Even.sum(1)

    Bins_Odd_Sum = Bin_0_Odd.sum(1) + Bin_1_Odd.sum(1) + Bin_2_Odd.sum(1) + Bin_3_Odd.sum(1) + Bin_4_Odd.sum(
        1) + Bin_5_Odd.sum(1) + Bin_6_Odd.sum(1)

    Bin_0_chi_sq = (((Bin_0_Even.sum(1) / Bins_Even_Sum) - (Bin_0_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_0_Even.sum(1) / Bins_Even_Sum) - (Bin_0_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_0_Even.sum(1) / Bins_Even_Sum))

    Bin_1_chi_sq = (((Bin_1_Even.sum(1) / Bins_Even_Sum) - (Bin_1_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_1_Even.sum(1) / Bins_Even_Sum) - (Bin_1_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_1_Even.sum(1) / Bins_Even_Sum))

    Bin_2_chi_sq = (((Bin_2_Even.sum(1) / Bins_Even_Sum) - (Bin_2_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_2_Even.sum(1) / Bins_Even_Sum) - (Bin_2_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_2_Even.sum(1) / Bins_Even_Sum))

    Bin_3_chi_sq = (((Bin_3_Even.sum(1) / Bins_Even_Sum) - (Bin_3_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_3_Even.sum(1) / Bins_Even_Sum) - (Bin_3_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_3_Even.sum(1) / Bins_Even_Sum))

    Bin_4_chi_sq = (((Bin_4_Even.sum(1) / Bins_Even_Sum) - (Bin_4_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_4_Even.sum(1) / Bins_Even_Sum) - (Bin_4_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_4_Even.sum(1) / Bins_Even_Sum))

    Bin_5_chi_sq = (((Bin_5_Even.sum(1) / Bins_Even_Sum) - (Bin_5_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_5_Even.sum(1) / Bins_Even_Sum) - (Bin_5_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_5_Even.sum(1) / Bins_Even_Sum))

    Bin_6_chi_sq = (((Bin_6_Even.sum(1) / Bins_Even_Sum) - (Bin_6_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_6_Even.sum(1) / Bins_Even_Sum) - (Bin_6_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_6_Even.sum(1) / Bins_Even_Sum))

    Chisquare_loss = Bin_0_chi_sq + Bin_1_chi_sq + Bin_2_chi_sq + Bin_3_chi_sq + Bin_4_chi_sq + Bin_5_chi_sq + Bin_6_chi_sq;

    loss_value = - (Chisquare_loss * torch.tensor(1.0).cuda())

    Bin_0_chi_sq.requires_grad_(True)
    Bin_1_chi_sq.requires_grad_(True)
    Bin_2_chi_sq.requires_grad_(True)
    Bin_3_chi_sq.requires_grad_(True)
    Bin_4_chi_sq.requires_grad_(True)
    Bin_5_chi_sq.requires_grad_(True)
    Bin_6_chi_sq.requires_grad_(True)

    Bins_Even_Sum.requires_grad_(True)
    loss_value.requires_grad_(True)
    Chisquare_loss.requires_grad_(True)
    Mean_Of_NLL_Value = torch.mean(loss_value)

    return Mean_Of_NLL_Value


def Take_Histogram(predicted_values, label_values):
    torch.index_select(output, dim=1, index=(torch.tensor([0])).cuda())

    Bin_0_LB = torch.ge(predicted_values, 0.0).float()
    Bin_0_UB = torch.lt(predicted_values, 0.142857).float()
    Bin_0 = torch.where(Bin_0_LB > 0, Bin_0_UB, Bin_0_LB)
    Bin_0_Even = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_0_Odd = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_1_LB = torch.ge(predicted_values, 0.142857).float()
    Bin_1_UB = torch.lt(predicted_values, 0.285714).float()
    Bin_1 = torch.where(Bin_1_LB > 0, Bin_1_UB, Bin_1_LB)
    Bin_1_Even = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_1_Odd = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_2_LB = torch.ge(predicted_values, 0.285714).float()
    Bin_2_UB = torch.lt(predicted_values, 0.428571).float()
    Bin_2 = torch.where(Bin_2_LB > 0, Bin_2_UB, Bin_2_LB)
    Bin_2_Even = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_2_Odd = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_3_LB = torch.ge(predicted_values, 0.428571).float()
    Bin_3_UB = torch.lt(predicted_values, 0.571428).float()
    Bin_3 = torch.where(Bin_3_LB > 0, Bin_3_UB, Bin_3_LB)
    Bin_3_Even = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_3_Odd = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_4_LB = torch.ge(predicted_values, 0.571428).float()
    Bin_4_UB = torch.lt(predicted_values, 0.714285).float()
    Bin_4 = torch.where(Bin_4_LB > 0, Bin_4_UB, Bin_4_LB)
    Bin_4_Even = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_4_Odd = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_5_LB = torch.ge(predicted_values, 0.714285).float()
    Bin_5_UB = torch.lt(predicted_values, 0.857142).float()
    Bin_5 = torch.where(Bin_5_LB > 0, Bin_5_UB, Bin_5_LB)
    Bin_5_Even = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_5_Odd = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_6_LB = torch.ge(predicted_values, 0.857142).float()
    Bin_6_UB = torch.lt(predicted_values, 1).float()
    Bin_6 = torch.where(Bin_6_LB > 0, Bin_6_UB, Bin_6_LB)
    Bin_6_Even = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_6_Odd = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_0_LB.requires_grad_(True)
    Bin_0_UB.requires_grad_(True)
    Bin_0.requires_grad_(True)
    Bin_0_Even.requires_grad_(True)
    Bin_0_Odd.requires_grad_(True)

    Bin_1_LB.requires_grad_(True)
    Bin_1_UB.requires_grad_(True)
    Bin_1.requires_grad_(True)
    Bin_1_Even.requires_grad_(True)
    Bin_1_Odd.requires_grad_(True)

    Bin_2_LB.requires_grad_(True)
    Bin_2_UB.requires_grad_(True)
    Bin_2.requires_grad_(True)
    Bin_2_Even.requires_grad_(True)
    Bin_2_Odd.requires_grad_(True)

    Bin_3_LB.requires_grad_(True)
    Bin_3_UB.requires_grad_(True)
    Bin_3.requires_grad_(True)
    Bin_3_Even.requires_grad_(True)
    Bin_3_Odd.requires_grad_(True)

    Bin_4_LB.requires_grad_(True)
    Bin_4_UB.requires_grad_(True)
    Bin_4.requires_grad_(True)
    Bin_4_Even.requires_grad_(True)
    Bin_4_Odd.requires_grad_(True)

    Bin_5_LB.requires_grad_(True)
    Bin_5_UB.requires_grad_(True)
    Bin_5.requires_grad_(True)
    Bin_5_Even.requires_grad_(True)
    Bin_5_Odd.requires_grad_(True)

    Bin_6_LB.requires_grad_(True)
    Bin_6_UB.requires_grad_(True)
    Bin_6.requires_grad_(True)
    Bin_6_Even.requires_grad_(True)
    Bin_6_Odd.requires_grad_(True)

    Bins_Even_Sum = Bin_0_Even.sum(1) + Bin_1_Even.sum(1) + Bin_2_Even.sum(1) + Bin_3_Even.sum(1) + Bin_4_Even.sum(
        1) + Bin_5_Even.sum(1) + Bin_6_Even.sum(1)

    Bins_Odd_Sum = Bin_0_Odd.sum(1) + Bin_1_Odd.sum(1) + Bin_2_Odd.sum(1) + Bin_3_Odd.sum(1) + Bin_4_Odd.sum(
        1) + Bin_5_Odd.sum(1) + Bin_6_Odd.sum(1)

    Even_Base = -(((Bin_0_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_0_Even.sum(1) / Bins_Even_Sum)) + (
            (Bin_1_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_1_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_2_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_2_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_3_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_3_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_4_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_4_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_5_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_5_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_6_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_6_Even.sum(1) / Bins_Even_Sum)))

    Odd_NLL = -(((Bin_0_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_0_Even.sum(1) / Bins_Even_Sum)) + (
            (Bin_1_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_1_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_2_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_2_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_3_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_3_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_4_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_4_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_5_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_5_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_6_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_6_Even.sum(1) / Bins_Even_Sum)))
    loss_value = Odd_NLL - Even_Base

    Bins_Even_Sum.requires_grad_(True)
    Even_Base.requires_grad_(True)
    Odd_NLL.requires_grad_(True)
    loss_value.requires_grad_(True)

    Mean_Of_NLL_Value = torch.mean(loss_value)

    Bins_info = tuple(
        [Bin_0_Even, Bin_1_Even, Bin_2_Even, Bin_3_Even, Bin_4_Even, Bin_5_Even, Bin_6_Even, Bin_0_Odd, Bin_1_Odd,
         Bin_2_Odd, Bin_3_Odd, Bin_4_Odd, Bin_5_Odd, Bin_6_Odd])

    # print("input_size = ",predicted_values.size() )
    # print("Bin0_size = ", Bin_0_Even.size())
    # # print("[0][n] = ", Bin_0)
    # print("[0][n] = ", Bin_3)
    # Weight_odd = torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())
    # print("Weight size = ", Weight_odd.size())
    # # print("Weight_Odd = ", Weight_odd)
    # print("Bin*Weight = ",(Bin_3* Weight_odd).size())

    # print("[0][n] = ", Bin_0.sum(1))
    # print("[1][n] = ", Bin_1.sum(1))

    # print("[2][n] = ", Bin_2.sum(1))
    # print("[3][n] = ", Bin_3.sum(1))
    # print("[4][n] = ", Bin_4.sum(1))
    # print("[5][n] = ", Bin_5.sum(1))
    # print("[6][n] = ", Bin_6.sum(1))

    #
    print("[0][Even] = ", Bin_0_Even.sum(1))
    print("[1][Even] = ", Bin_1_Even.sum(1))
    print("[2][Even] = ", Bin_2_Even.sum(1))
    print("[3][Even] = ", Bin_3_Even.sum(1))
    print("[4][Even] = ", Bin_4_Even.sum(1))
    print("[5][Even] = ", Bin_5_Even.sum(1))
    print("[6][Even] = ", Bin_6_Even.sum(1))

    print("[0][Odd] = ", Bin_0_Odd.sum(1))
    print("[1][Odd] = ", Bin_1_Odd.sum(1))
    print("[2][Odd] = ", Bin_2_Odd.sum(1))
    print("[3][Odd] = ", Bin_3_Odd.sum(1))
    print("[4][Odd] = ", Bin_4_Odd.sum(1))
    print("[5][Odd] = ", Bin_5_Odd.sum(1))
    print("[6][Odd] = ", Bin_6_Odd.sum(1))

    return Bins_info


def Take_first(input_vector):
    return torch.index_select(input_vector, dim=1, index=(torch.tensor([0])).cuda())


def Take_second(input_vector):
    return torch.index_select(input_vector, dim=1, index=(torch.tensor([1])).cuda())


def my_mse_loss(x, y):
    return torch.mean(x - y)


net = Net()
# net = torch.load('./Train_with_59_and_bin_lb_2/Epoch_5400_Phistar_model.pkl')
#
# net.cuda()
# net.train()
#
# optimizer = optim.SGD(net.parameters(), lr=0.0001)

My_new_dataset = My_1p1p_Dataset()

train_loader2 = DataLoader(dataset=My_new_dataset,
                           batch_size=Batch_size_of_NN,
                           shuffle=False)

# print("Training loop:")
# for idx in range(0, EPOCHS_TO_TRAIN):
#     for input, target in zip(inputs, targets):
#         optimizer.zero_grad()   # zero the gradient buffers
#         output = net(input)
#         loss = criterion(output, target)
#         loss.backward()
#         optimizer.step()    # Does the update
# if idx % 5000 == 0:
# print("Epoch {: >8} Loss: {}".format(idx, loss.data.numpy()[0]))


for i_file in range(201):

    # file_name = "./Saved_model/Epoch_"+str(i_file*50)+"_Phistar_model.pkl"
    # print(file_name)
    net = torch.load("./Train_with_59_and_bin_lb_2_02/Epoch_"+str(i_file*50)+"_Phistar_model.pkl")
    net.cuda()
    # net.train()
    net.eval()

    result_array = np.array([])

    for i, data in enumerate(train_loader2):
        # 将数据从 train_loader 中读出来,一次读取的样本数是32个
        inputs, labels = data

        # 将这些数据转换成Variable类型
        inputs, labels = Variable(inputs), Variable(labels)
        inputs, labels = inputs.cuda(), labels.cuda()

        # 接下来就是跑模型的环节了，我们这里使用print来代替
        # print("epoch：", epoch, "的第" , i, "个inputs", inputs.data.size(), "labels", labels.data.size())
        # optimizer.zero_grad()  # zero the gradient buffers
        output = net(inputs)
        # loss = criterion(output, Take_first(labels))
        loss = Cal_fixed_dnll_loss_with_bin_lower_bound(output, labels)
        result_array = np.append(result_array, loss.cpu().detach().numpy())



    Loss_mean = result_array.mean()
    Loss_std = result_array.std()
    print("epoch: ", i_file * 50, "  loss mean: ", Loss_mean, "  loss std: ", Loss_std)

print("")
print("Final results:")
