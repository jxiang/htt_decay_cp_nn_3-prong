# NN based Htt observable training

This is a tool for training an observable for H-tautau decay CP analysis


# Download the package

Fork  [htt_decay_cp_nn_3-prong](https://gitlab.cern.ch/jxiang/htt_decay_cp_nn_3-prong)  on GitLab

Then run

```
git clone ssh://git@gitlab.cern.ch:7999/[username]/htt_decay_cp_nn_3-prong.git
```


## Tools Description


Python_ROOT_Elz_sample_converter

A simple tool converting ASCII file from Elzbieta to Root. Require ROOT and corresponding python


```

python Create_root_py2.py
python Create_root_py2_ztt.py


```



C_Root_ATLAS_converter

Converting ROOT file converted from Elzbieta to an ATLAS form root file. wrote in C++.


```

root -b Convert_to_ATLAS.cxx


```


Python3_NN_Training

It is a short Neural Network training code designed to train a model that maximize separation between the distribution of observable in CP-Even and CP-Odd case. It requires Python3.7 or higher version and Latest Pytorch.

The model is defaultly trained on GPU, so it requires CUDA 10.1 or higher. It can also be trained in CPU by removing the .CUDA() in the code. In this case there is no CUDA requirement. 

I will upload CPU version code later.

In the sample code, it defaultly using chi-square loss (multiply by 1000).


```

python Task_84_Train_c_chi_sq_1000.py >> output_loss.log



```

Python3_NN_Test

This code has same requirement with Python3_NN_Training. 

Defaultly, it print the epoch and loss value(mean and std) directly.

```

python Task_78_Test_square_loss_on_dcroentro_trained_01.py >> output_loss.log


```

Some samples for Htt machine learning analysis has been uploaded to the public folder below.

/afs/cern.ch/work/j/jxiang/public/Htt_signal_ggH_with_trackcharge/

```


There folder contain following samples:

VBF/ggH/Ztt: Output of xTauFW, containing the charge of pion tracks. 

Prepared_sample_for_training: Sample for my pytorch training code.

Prepared_sample_for_testing: Sample for my pytorch testing code.

sample_after_converting_Elz_to_Root: Root format of Elzbieta's sample

sample_converted_to_xTauFW_format: Root file of Elzbieta'sample, in xTauFW output format.
```









