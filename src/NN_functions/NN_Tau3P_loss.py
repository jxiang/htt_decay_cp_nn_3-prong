import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import json

from torch.utils.data import Dataset
from torch.utils.data import DataLoader

def Cal_loss_DNLL(predicted_values, label_values):
    Test_L1 = nn.Linear(1, 1, True)
    nn.init.ones_(Test_L1.weight)
    Test_L1.bias.data.fill_(0)
    # Test_Out = F.sigmoid(torch.tensor(10)*F.selu(Test_L1(predicted_values-torch.tensor(3))))
    # Test_Out = torch.log(Test_L1(predicted_values))
    # Test_Out = torch.log(predicted_values)
    # New_test = torch.log(predicted_values)
    # Test_Out = F.sigmoid(New_test) * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(0.00001)
    # print("input = ", predicted_values)

    # Test_Out.requires_grad_(True)
    # Test_Out.retain_grad()
    # Test_Out = F.sigmoid(F.selu(torch.tensor(10)*(predicted_values-torch.tensor(0.5))))
    # Test_input = torch.tensor(0.49)
    # Test_Function =F.sigmoid(torch.tensor(100)*F.selu(torch.tensor(0.1)))
    # print("Test_function = ", Test_Function)
    # print("output = ", torch.index_select(Test_Out, dim=1, index=(torch.tensor([0])).cuda()))

    # Bin_0_LB = torch.ge(predicted_values, 0.0).float()
    # Bin_0_UB = torch.lt(predicted_values, 0.142857).float()
    # Bin_0 = torch.where(Bin_0_LB > 0, Bin_0_UB, Bin_0_LB)
    Bin_0 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.0)) * (torch.tensor(0.142857) - predicted_values)))

    # print(Bin_0.device)
    Bin_0_Even = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    # print(Bin_0_Even.device)
    Bin_0_Odd = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_1_LB = torch.ge(predicted_values, 0.142857).float()
    # Bin_1_UB = torch.lt(predicted_values, 0.285714).float()
    Bin_1 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.142857)) * (torch.tensor(0.285714) - predicted_values)))
    Bin_1_Even = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_1_Odd = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_2_LB = torch.ge(predicted_values, 0.285714).float()
    # Bin_2_UB = torch.lt(predicted_values, 0.428571).float()
    Bin_2 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.285714)) * (torch.tensor(0.428571) - predicted_values)))
    Bin_2_Even = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_2_Odd = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_3_LB = torch.ge(predicted_values, 0.428571).float()
    # Bin_3_UB = torch.lt(predicted_values, 0.571428).float()
    Bin_3 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.428571)) * (torch.tensor(0.571428) - predicted_values)))
    Bin_3_Even = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_3_Odd = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_4_LB = torch.ge(predicted_values, 0.571428).float()
    # Bin_4_UB = torch.lt(predicted_values, 0.714285).float()
    Bin_4 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.571428)) * (torch.tensor(0.714285) - predicted_values)))
    Bin_4_Even = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_4_Odd = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_5_LB = torch.ge(predicted_values, 0.714285).float()
    # Bin_5_UB = torch.lt(predicted_values, 0.857142).float()
    Bin_5 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.714285)) * (torch.tensor(0.857142) - predicted_values)))
    Bin_5_Even = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_5_Odd = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_6_LB = torch.ge(predicted_values, 0.857142).float()
    # Bin_6_UB = torch.lt(predicted_values, 1).float()
    Bin_6 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.857142)) * (torch.tensor(1.0) - predicted_values)))
    Bin_6_Even = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_6_Odd = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    stirling_constant = torch.tensor(2.50662827)
    log_stirling_constant = torch.log(stirling_constant)

    Bin_0_Even_sum = Bin_0_Even.sum(1)
    Bin_0_Odd_sum = Bin_0_Odd.sum(1)

    Bin_1_Even_sum = Bin_1_Even.sum(1)
    Bin_1_Odd_sum = Bin_1_Odd.sum(1)

    Bin_2_Even_sum = Bin_2_Even.sum(1)
    Bin_2_Odd_sum = Bin_2_Odd.sum(1)

    Bin_3_Even_sum = Bin_3_Even.sum(1)
    Bin_3_Odd_sum = Bin_3_Odd.sum(1)

    Bin_4_Even_sum = Bin_4_Even.sum(1)
    Bin_4_Odd_sum = Bin_4_Odd.sum(1)

    Bin_5_Even_sum = Bin_5_Even.sum(1)
    Bin_5_Odd_sum = Bin_5_Odd.sum(1)

    Bin_6_Even_sum = Bin_6_Even.sum(1)
    Bin_6_Odd_sum = Bin_6_Odd.sum(1)

    LogLikelihood_Odd_Even_Bin_0 = (Bin_0_Odd_sum - Bin_0_Even_sum) + Bin_0_Odd_sum * torch.log(
        Bin_0_Even_sum) - log_stirling_constant - (
                                           Bin_0_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_0_Odd_sum)

    LogLikelihood_Odd_Even_Bin_1 = (Bin_1_Odd_sum - Bin_1_Even_sum) + Bin_1_Odd_sum * torch.log(
        Bin_1_Even_sum) - log_stirling_constant - (
                                           Bin_1_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_1_Odd_sum)

    LogLikelihood_Odd_Even_Bin_2 = (Bin_2_Odd_sum - Bin_2_Even_sum) + Bin_2_Odd_sum * torch.log(
        Bin_2_Even_sum) - log_stirling_constant - (
                                           Bin_2_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_2_Odd_sum)

    LogLikelihood_Odd_Even_Bin_3 = (Bin_3_Odd_sum - Bin_3_Even_sum) + Bin_3_Odd_sum * torch.log(
        Bin_3_Even_sum) - log_stirling_constant - (
                                           Bin_3_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_3_Odd_sum)

    LogLikelihood_Odd_Even_Bin_4 = (Bin_4_Odd_sum - Bin_4_Even_sum) + Bin_4_Odd_sum * torch.log(
        Bin_4_Even_sum) - log_stirling_constant - (
                                           Bin_4_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_4_Odd_sum)

    LogLikelihood_Odd_Even_Bin_5 = (Bin_5_Odd_sum - Bin_5_Even_sum) + Bin_5_Odd_sum * torch.log(
        Bin_5_Even_sum) - log_stirling_constant - (
                                           Bin_5_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_5_Odd_sum)

    LogLikelihood_Odd_Even_Bin_6 = (Bin_6_Odd_sum - Bin_6_Even_sum) + Bin_6_Odd_sum * torch.log(
        Bin_6_Even_sum) - log_stirling_constant - (
                                           Bin_6_Odd_sum + torch.tensor(0.5)) * torch.log(Bin_6_Odd_sum)

    LogLikelihood_Even_Even_Bin_0 = (Bin_0_Even_sum - Bin_0_Even_sum) + Bin_0_Even_sum * torch.log(
        Bin_0_Even_sum) - log_stirling_constant - (
                                            Bin_0_Even_sum + torch.tensor(0.5)) * torch.log(Bin_0_Even_sum)

    LogLikelihood_Even_Even_Bin_1 = (Bin_1_Even_sum - Bin_1_Even_sum) + Bin_1_Even_sum * torch.log(
        Bin_1_Even_sum) - log_stirling_constant - (
                                            Bin_1_Even_sum + torch.tensor(0.5)) * torch.log(Bin_1_Even_sum)

    LogLikelihood_Even_Even_Bin_2 = (Bin_2_Even_sum - Bin_2_Even_sum) + Bin_2_Even_sum * torch.log(
        Bin_2_Even_sum) - log_stirling_constant - (
                                            Bin_2_Even_sum + torch.tensor(0.5)) * torch.log(Bin_2_Even_sum)

    LogLikelihood_Even_Even_Bin_3 = (Bin_3_Even_sum - Bin_3_Even_sum) + Bin_3_Even_sum * torch.log(
        Bin_3_Even_sum) - log_stirling_constant - (
                                            Bin_3_Even_sum + torch.tensor(0.5)) * torch.log(Bin_3_Even_sum)

    LogLikelihood_Even_Even_Bin_4 = (Bin_4_Even_sum - Bin_4_Even_sum) + Bin_4_Even_sum * torch.log(
        Bin_4_Even_sum) - log_stirling_constant - (
                                            Bin_4_Even_sum + torch.tensor(0.5)) * torch.log(Bin_4_Even_sum)

    LogLikelihood_Even_Even_Bin_5 = (Bin_5_Even_sum - Bin_5_Even_sum) + Bin_5_Even_sum * torch.log(
        Bin_5_Even_sum) - log_stirling_constant - (
                                            Bin_5_Even_sum + torch.tensor(0.5)) * torch.log(Bin_5_Even_sum)

    LogLikelihood_Even_Even_Bin_6 = (Bin_6_Even_sum - Bin_6_Even_sum) + Bin_6_Even_sum * torch.log(
        Bin_6_Even_sum) - log_stirling_constant - (
                                            Bin_6_Even_sum + torch.tensor(0.5)) * torch.log(Bin_6_Even_sum)

    Bin_num_lb = torch.tensor(2.0).cuda()
    Delta_LogLikelihood_Bin_0 = (LogLikelihood_Odd_Even_Bin_0 - LogLikelihood_Even_Even_Bin_0) * F.sigmoid(
        Bin_0_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_0_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_1 = (LogLikelihood_Odd_Even_Bin_1 - LogLikelihood_Even_Even_Bin_1) * F.sigmoid(
        Bin_1_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_1_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_2 = (LogLikelihood_Odd_Even_Bin_2 - LogLikelihood_Even_Even_Bin_2) * F.sigmoid(
        Bin_2_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_2_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_3 = (LogLikelihood_Odd_Even_Bin_3 - LogLikelihood_Even_Even_Bin_3) * F.sigmoid(
        Bin_3_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_3_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_4 = (LogLikelihood_Odd_Even_Bin_4 - LogLikelihood_Even_Even_Bin_4) * F.sigmoid(
        Bin_4_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_4_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_5 = (LogLikelihood_Odd_Even_Bin_5 - LogLikelihood_Even_Even_Bin_5) * F.sigmoid(
        Bin_5_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_5_Even_sum - Bin_num_lb)
    Delta_LogLikelihood_Bin_6 = (LogLikelihood_Odd_Even_Bin_6 - LogLikelihood_Even_Even_Bin_6) * F.sigmoid(
        Bin_6_Odd_sum - Bin_num_lb) * F.sigmoid(Bin_6_Even_sum - Bin_num_lb)

    Fixed_DLL = Delta_LogLikelihood_Bin_0 + Delta_LogLikelihood_Bin_1 + Delta_LogLikelihood_Bin_2 + Delta_LogLikelihood_Bin_3 + Delta_LogLikelihood_Bin_4 + Delta_LogLikelihood_Bin_5 + Delta_LogLikelihood_Bin_6

    # Fixed_DLL = LogLikelihood_Odd_Even_Bin_0 + LogLikelihood_Odd_Even_Bin_1 + LogLikelihood_Odd_Even_Bin_2 + LogLikelihood_Odd_Even_Bin_3 + LogLikelihood_Odd_Even_Bin_4 + LogLikelihood_Odd_Even_Bin_5 + LogLikelihood_Odd_Even_Bin_6 - (
    #         LogLikelihood_Even_Even_Bin_0 + LogLikelihood_Even_Even_Bin_1 + LogLikelihood_Even_Even_Bin_2 + LogLikelihood_Even_Even_Bin_3 + LogLikelihood_Even_Even_Bin_4 + LogLikelihood_Even_Even_Bin_5 + LogLikelihood_Even_Even_Bin_6)
    Fixed_DNLL = - Fixed_DLL
    # print(predicted_values)

    # Bin_lower_bound = torch.tensor(100.0).cuda()
    # Loss_Scale_when_below_lb = torch.tensor(100.0).cuda()

    # Loss_with_bin_lb = Fixed_DLL + Loss_Scale_when_below_lb * F.sigmoid(
    #     F.sigmoid(Bin_lower_bound - Bin_0_Even_sum) + F.sigmoid(Bin_lower_bound - Bin_1_Even_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_2_Even_sum) + F.sigmoid(Bin_lower_bound - Bin_3_Even_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_4_Even_sum) + F.sigmoid(Bin_lower_bound - Bin_5_Even_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_6_Even_sum) + F.sigmoid(Bin_lower_bound - Bin_0_Odd_sum) + F.sigmoid(Bin_lower_bound - Bin_1_Odd_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_2_Odd_sum) + F.sigmoid(Bin_lower_bound - Bin_3_Odd_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_4_Odd_sum) + F.sigmoid(Bin_lower_bound - Bin_5_Odd_sum) + F.sigmoid(
    #         Bin_lower_bound - Bin_6_Odd_sum) - torch.tensor(0.5).cuda())

    output = Fixed_DLL
    output.requires_grad_(True)
    Mean_Of_NLL_Value = torch.mean(output)

    return Mean_Of_NLL_Value



def Cal_loss_chisquare(predicted_values, label_values):
    Test_L1 = nn.Linear(1, 1, True)
    nn.init.ones_(Test_L1.weight)
    Test_L1.bias.data.fill_(0)
    # Test_Out = F.sigmoid(torch.tensor(10)*F.selu(Test_L1(predicted_values-torch.tensor(3))))
    # Test_Out = torch.log(Test_L1(predicted_values))
    # Test_Out = torch.log(predicted_values)
    # New_test = torch.log(predicted_values)
    # Test_Out = F.sigmoid(New_test) * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(0.00001)
    # print("input = ", predicted_values)

    # Test_Out.requires_grad_(True)
    # Test_Out.retain_grad()
    # Test_Out = F.sigmoid(F.selu(torch.tensor(10)*(predicted_values-torch.tensor(0.5))))
    # Test_input = torch.tensor(0.49)
    # Test_Function =F.sigmoid(torch.tensor(100)*F.selu(torch.tensor(0.1)))
    # print("Test_function = ", Test_Function)
    # print("output = ", torch.index_select(Test_Out, dim=1, index=(torch.tensor([0])).cuda()))

    # Bin_0_LB = torch.ge(predicted_values, 0.0).float()
    # Bin_0_UB = torch.lt(predicted_values, 0.142857).float()
    # Bin_0 = torch.where(Bin_0_LB > 0, Bin_0_UB, Bin_0_LB)
    Bin_0 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.0)) * (torch.tensor(0.142857) - predicted_values)))

    # print(Bin_0.device)
    Bin_0_Even = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    # print(Bin_0_Even.device)
    Bin_0_Odd = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_1_LB = torch.ge(predicted_values, 0.142857).float()
    # Bin_1_UB = torch.lt(predicted_values, 0.285714).float()
    Bin_1 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.142857)) * (torch.tensor(0.285714) - predicted_values)))
    Bin_1_Even = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_1_Odd = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_2_LB = torch.ge(predicted_values, 0.285714).float()
    # Bin_2_UB = torch.lt(predicted_values, 0.428571).float()
    Bin_2 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.285714)) * (torch.tensor(0.428571) - predicted_values)))
    Bin_2_Even = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_2_Odd = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_3_LB = torch.ge(predicted_values, 0.428571).float()
    # Bin_3_UB = torch.lt(predicted_values, 0.571428).float()
    Bin_3 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.428571)) * (torch.tensor(0.571428) - predicted_values)))
    Bin_3_Even = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_3_Odd = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_4_LB = torch.ge(predicted_values, 0.571428).float()
    # Bin_4_UB = torch.lt(predicted_values, 0.714285).float()
    Bin_4 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.571428)) * (torch.tensor(0.714285) - predicted_values)))
    Bin_4_Even = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_4_Odd = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_5_LB = torch.ge(predicted_values, 0.714285).float()
    # Bin_5_UB = torch.lt(predicted_values, 0.857142).float()
    Bin_5 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.714285)) * (torch.tensor(0.857142) - predicted_values)))
    Bin_5_Even = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_5_Odd = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    # Bin_6_LB = torch.ge(predicted_values, 0.857142).float()
    # Bin_6_UB = torch.lt(predicted_values, 1).float()
    Bin_6 = F.sigmoid(torch.tensor(100) * F.selu(
        (predicted_values - torch.tensor(0.857142)) * (torch.tensor(1.0) - predicted_values)))
    Bin_6_Even = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001).cuda()
    Bin_6_Odd = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bins_Even_Sum = Bin_0_Even.sum(1) + Bin_1_Even.sum(1) + Bin_2_Even.sum(1) + Bin_3_Even.sum(1) + Bin_4_Even.sum(
        1) + Bin_5_Even.sum(1) + Bin_6_Even.sum(1)

    Bins_Odd_Sum = Bin_0_Odd.sum(1) + Bin_1_Odd.sum(1) + Bin_2_Odd.sum(1) + Bin_3_Odd.sum(1) + Bin_4_Odd.sum(
        1) + Bin_5_Odd.sum(1) + Bin_6_Odd.sum(1)

    Bin_0_chi_sq = (((Bin_0_Even.sum(1) / Bins_Even_Sum) - (Bin_0_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_0_Even.sum(1) / Bins_Even_Sum) - (Bin_0_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_0_Even.sum(1) / Bins_Even_Sum))

    Bin_1_chi_sq = (((Bin_1_Even.sum(1) / Bins_Even_Sum) - (Bin_1_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_1_Even.sum(1) / Bins_Even_Sum) - (Bin_1_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_1_Even.sum(1) / Bins_Even_Sum))

    Bin_2_chi_sq = (((Bin_2_Even.sum(1) / Bins_Even_Sum) - (Bin_2_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_2_Even.sum(1) / Bins_Even_Sum) - (Bin_2_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_2_Even.sum(1) / Bins_Even_Sum))

    Bin_3_chi_sq = (((Bin_3_Even.sum(1) / Bins_Even_Sum) - (Bin_3_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_3_Even.sum(1) / Bins_Even_Sum) - (Bin_3_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_3_Even.sum(1) / Bins_Even_Sum))

    Bin_4_chi_sq = (((Bin_4_Even.sum(1) / Bins_Even_Sum) - (Bin_4_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_4_Even.sum(1) / Bins_Even_Sum) - (Bin_4_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_4_Even.sum(1) / Bins_Even_Sum))

    Bin_5_chi_sq = (((Bin_5_Even.sum(1) / Bins_Even_Sum) - (Bin_5_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_5_Even.sum(1) / Bins_Even_Sum) - (Bin_5_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_5_Even.sum(1) / Bins_Even_Sum))

    Bin_6_chi_sq = (((Bin_6_Even.sum(1) / Bins_Even_Sum) - (Bin_6_Odd.sum(1) / Bins_Odd_Sum)) * (
            (Bin_6_Even.sum(1) / Bins_Even_Sum) - (Bin_6_Odd.sum(1) / Bins_Odd_Sum))) / (
                       (Bin_6_Even.sum(1) / Bins_Even_Sum))

    Chisquare_loss = Bin_0_chi_sq + Bin_1_chi_sq + Bin_2_chi_sq + Bin_3_chi_sq + Bin_4_chi_sq + Bin_5_chi_sq + Bin_6_chi_sq;

    loss_value = - (Chisquare_loss * torch.tensor(1000.0).cuda())

    Bin_0_chi_sq.requires_grad_(True)
    Bin_1_chi_sq.requires_grad_(True)
    Bin_2_chi_sq.requires_grad_(True)
    Bin_3_chi_sq.requires_grad_(True)
    Bin_4_chi_sq.requires_grad_(True)
    Bin_5_chi_sq.requires_grad_(True)
    Bin_6_chi_sq.requires_grad_(True)

    Bins_Even_Sum.requires_grad_(True)
    loss_value.requires_grad_(True)
    Chisquare_loss.requires_grad_(True)
    Mean_Of_NLL_Value = torch.mean(loss_value)

    return Mean_Of_NLL_Value

