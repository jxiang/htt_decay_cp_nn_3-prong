import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import json

from torch.utils.data import Dataset
from torch.utils.data import DataLoader


def Take_Histogram(predicted_values, label_values):
    torch.index_select(output, dim=1, index=(torch.tensor([0])).cuda())

    Bin_0_LB = torch.ge(predicted_values, 0.0).float()
    Bin_0_UB = torch.lt(predicted_values, 0.142857).float()
    Bin_0 = torch.where(Bin_0_LB > 0, Bin_0_UB, Bin_0_LB)
    Bin_0_Even = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_0_Odd = Bin_0 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_1_LB = torch.ge(predicted_values, 0.142857).float()
    Bin_1_UB = torch.lt(predicted_values, 0.285714).float()
    Bin_1 = torch.where(Bin_1_LB > 0, Bin_1_UB, Bin_1_LB)
    Bin_1_Even = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_1_Odd = Bin_1 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_2_LB = torch.ge(predicted_values, 0.285714).float()
    Bin_2_UB = torch.lt(predicted_values, 0.428571).float()
    Bin_2 = torch.where(Bin_2_LB > 0, Bin_2_UB, Bin_2_LB)
    Bin_2_Even = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_2_Odd = Bin_2 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_3_LB = torch.ge(predicted_values, 0.428571).float()
    Bin_3_UB = torch.lt(predicted_values, 0.571428).float()
    Bin_3 = torch.where(Bin_3_LB > 0, Bin_3_UB, Bin_3_LB)
    Bin_3_Even = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_3_Odd = Bin_3 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_4_LB = torch.ge(predicted_values, 0.571428).float()
    Bin_4_UB = torch.lt(predicted_values, 0.714285).float()
    Bin_4 = torch.where(Bin_4_LB > 0, Bin_4_UB, Bin_4_LB)
    Bin_4_Even = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_4_Odd = Bin_4 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_5_LB = torch.ge(predicted_values, 0.714285).float()
    Bin_5_UB = torch.lt(predicted_values, 0.857142).float()
    Bin_5 = torch.where(Bin_5_LB > 0, Bin_5_UB, Bin_5_LB)
    Bin_5_Even = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_5_Odd = Bin_5 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_6_LB = torch.ge(predicted_values, 0.857142).float()
    Bin_6_UB = torch.lt(predicted_values, 1).float()
    Bin_6 = torch.where(Bin_6_LB > 0, Bin_6_UB, Bin_6_LB)
    Bin_6_Even = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([0])).cuda()) + torch.tensor(
        0.00001)
    Bin_6_Odd = Bin_6 * torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())

    Bin_0_LB.requires_grad_(True)
    Bin_0_UB.requires_grad_(True)
    Bin_0.requires_grad_(True)
    Bin_0_Even.requires_grad_(True)
    Bin_0_Odd.requires_grad_(True)

    Bin_1_LB.requires_grad_(True)
    Bin_1_UB.requires_grad_(True)
    Bin_1.requires_grad_(True)
    Bin_1_Even.requires_grad_(True)
    Bin_1_Odd.requires_grad_(True)

    Bin_2_LB.requires_grad_(True)
    Bin_2_UB.requires_grad_(True)
    Bin_2.requires_grad_(True)
    Bin_2_Even.requires_grad_(True)
    Bin_2_Odd.requires_grad_(True)

    Bin_3_LB.requires_grad_(True)
    Bin_3_UB.requires_grad_(True)
    Bin_3.requires_grad_(True)
    Bin_3_Even.requires_grad_(True)
    Bin_3_Odd.requires_grad_(True)

    Bin_4_LB.requires_grad_(True)
    Bin_4_UB.requires_grad_(True)
    Bin_4.requires_grad_(True)
    Bin_4_Even.requires_grad_(True)
    Bin_4_Odd.requires_grad_(True)

    Bin_5_LB.requires_grad_(True)
    Bin_5_UB.requires_grad_(True)
    Bin_5.requires_grad_(True)
    Bin_5_Even.requires_grad_(True)
    Bin_5_Odd.requires_grad_(True)

    Bin_6_LB.requires_grad_(True)
    Bin_6_UB.requires_grad_(True)
    Bin_6.requires_grad_(True)
    Bin_6_Even.requires_grad_(True)
    Bin_6_Odd.requires_grad_(True)

    Bins_Even_Sum = Bin_0_Even.sum(1) + Bin_1_Even.sum(1) + Bin_2_Even.sum(1) + Bin_3_Even.sum(1) + Bin_4_Even.sum(
        1) + Bin_5_Even.sum(1) + Bin_6_Even.sum(1)

    Bins_Odd_Sum = Bin_0_Odd.sum(1) + Bin_1_Odd.sum(1) + Bin_2_Odd.sum(1) + Bin_3_Odd.sum(1) + Bin_4_Odd.sum(
        1) + Bin_5_Odd.sum(1) + Bin_6_Odd.sum(1)

    Even_Base = -(((Bin_0_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_0_Even.sum(1) / Bins_Even_Sum)) + (
            (Bin_1_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_1_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_2_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_2_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_3_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_3_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_4_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_4_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_5_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_5_Even.sum(1) / Bins_Even_Sum)) + (
                          (Bin_6_Even.sum(1) / Bins_Even_Sum) * torch.log(Bin_6_Even.sum(1) / Bins_Even_Sum)))

    Odd_NLL = -(((Bin_0_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_0_Even.sum(1) / Bins_Even_Sum)) + (
            (Bin_1_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_1_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_2_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_2_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_3_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_3_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_4_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_4_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_5_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_5_Even.sum(1) / Bins_Even_Sum)) + (
                        (Bin_6_Odd.sum(1) / Bins_Odd_Sum) * torch.log(Bin_6_Even.sum(1) / Bins_Even_Sum)))
    loss_value = Odd_NLL - Even_Base

    Bins_Even_Sum.requires_grad_(True)
    Even_Base.requires_grad_(True)
    Odd_NLL.requires_grad_(True)
    loss_value.requires_grad_(True)

    Mean_Of_NLL_Value = torch.mean(loss_value)

    Bins_info = tuple(
        [Bin_0_Even, Bin_1_Even, Bin_2_Even, Bin_3_Even, Bin_4_Even, Bin_5_Even, Bin_6_Even, Bin_0_Odd, Bin_1_Odd,
         Bin_2_Odd, Bin_3_Odd, Bin_4_Odd, Bin_5_Odd, Bin_6_Odd])

    # print("input_size = ",predicted_values.size() )
    # print("Bin0_size = ", Bin_0_Even.size())
    # # print("[0][n] = ", Bin_0)
    # print("[0][n] = ", Bin_3)
    # Weight_odd = torch.index_select(label_values, dim=2, index=(torch.tensor([1])).cuda())
    # print("Weight size = ", Weight_odd.size())
    # # print("Weight_Odd = ", Weight_odd)
    # print("Bin*Weight = ",(Bin_3* Weight_odd).size())

    # print("[0][n] = ", Bin_0.sum(1))
    # print("[1][n] = ", Bin_1.sum(1))

    # print("[2][n] = ", Bin_2.sum(1))
    # print("[3][n] = ", Bin_3.sum(1))
    # print("[4][n] = ", Bin_4.sum(1))
    # print("[5][n] = ", Bin_5.sum(1))
    # print("[6][n] = ", Bin_6.sum(1))

    #
    print("[0][Even] = ", Bin_0_Even.sum(1))
    print("[1][Even] = ", Bin_1_Even.sum(1))
    print("[2][Even] = ", Bin_2_Even.sum(1))
    print("[3][Even] = ", Bin_3_Even.sum(1))
    print("[4][Even] = ", Bin_4_Even.sum(1))
    print("[5][Even] = ", Bin_5_Even.sum(1))
    print("[6][Even] = ", Bin_6_Even.sum(1))

    print("[0][Odd] = ", Bin_0_Odd.sum(1))
    print("[1][Odd] = ", Bin_1_Odd.sum(1))
    print("[2][Odd] = ", Bin_2_Odd.sum(1))
    print("[3][Odd] = ", Bin_3_Odd.sum(1))
    print("[4][Odd] = ", Bin_4_Odd.sum(1))
    print("[5][Odd] = ", Bin_5_Odd.sum(1))
    print("[6][Odd] = ", Bin_6_Odd.sum(1))

    return Bins_info

