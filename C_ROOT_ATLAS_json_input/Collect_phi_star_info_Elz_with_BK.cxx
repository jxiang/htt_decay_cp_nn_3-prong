#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#include <chrono>
#include <glob.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <fstream>


int Num_of_Events_in_an_input = 300;
int Number_of_inputs = 150;
int Number_of_signal_inputs = 100;
int Number_of_background_inputs = 200;

//using namespace TMVA;
using namespace std;
using namespace chrono;

//bool sortcol( const vector<float>& v1,const vector<float>& v2 ) {
//    return v1[0] > v2[0];
//}



vector <string> glob(const char *pattern) {
    glob_t g;
    glob(pattern, GLOB_TILDE, nullptr, &g); // one should ensure glob returns 0!
    vector <string> filelist;
    filelist.reserve(g.gl_pathc);
    for (size_t i = 0; i < g.gl_pathc; ++i) {
        filelist.emplace_back(g.gl_pathv[i]);
    }
    globfree(&g);
    return filelist;
}





TVector3
CalculateImpactParamVec(float T_d0, float T_z0, float T_VX, float T_VY, float T_VZ, float T_Phi, float PV_X, float PV_Y,
                        float PV_Z) {

    TVector3 pionVector;
    TVector3 initialPoint;
    TVector3 primaryVertex;
    TVector3 IPVector;


    pionVector.SetXYZ(T_VX, T_VY, T_VZ);
    initialPoint.SetXYZ(-T_d0 * sin(T_Phi), T_d0 * cos(T_Phi), T_z0);
    primaryVertex.SetXYZ(PV_X, PV_Y, PV_Z);
    double k = (primaryVertex - initialPoint) * pionVector / pionVector.Mag2();
    IPVector = initialPoint + k * pionVector - primaryVertex;

    return IPVector;


}


TVector3
CalculateImpactParamVec_Truth(float Truth_prod_vtx_x, float Truth_prod_vtx_y, float Truth_prod_vtx_z,
                              float Truth_dec_vtx_x, float Truth_dec_vtx_y, float Truth_dec_vtx_z,
                              const TLorentzVector &Decay_Charged) {

    TVector3 pionVector;
    TVector3 initialPoint;
    TVector3 primaryVertex;
    TVector3 IPVector;


    pionVector = Decay_Charged.Vect();
    initialPoint.SetXYZ(Truth_dec_vtx_x, Truth_dec_vtx_y, Truth_dec_vtx_z);
    primaryVertex.SetXYZ(Truth_prod_vtx_x, Truth_prod_vtx_y, Truth_prod_vtx_z);
    double k = (primaryVertex - initialPoint) * pionVector / pionVector.Mag2();
    IPVector = initialPoint + k * pionVector - primaryVertex;

    return IPVector;


}


float Calculate_angle_between_TVectors(const TVector3 &TV_0, const TVector3 &TV_1) {


    float Angle = TV_0.Angle(TV_1);
    return Angle;


}

int Check_Loose_charge_Only(float Charge_Product) {

    if (Charge_Product < 0) {
        return 1;
    } else {
        return 0;
    }

}


float Acoplanarity_IP_rho(TLorentzVector track, TLorentzVector ipVector_track, TLorentzVector charged_rho,
                          TLorentzVector neutral_rho,
                          const TLorentzVector &referenceFrame, bool track_is_plus) {
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    track.Boost(boostIntoReferenceFrame);
    ipVector_track.Boost(boostIntoReferenceFrame);
    charged_rho.Boost(boostIntoReferenceFrame);
    neutral_rho.Boost(boostIntoReferenceFrame);

    // IP vector
    TVector3 ip3Vector_track = ipVector_track.Vect();

    // parallel projection of impact vector onto track direction
    TVector3 ipVectorParallel_track = (ip3Vector_track * track.Vect().Unit()) * track.Vect().Unit();
    // Perpendicular component of impact vector
    TVector3 ipVectorPerp_track = (ip3Vector_track - ipVectorParallel_track).Unit();

    // Parallel projections of pi0 onto pi+/- direction
    TVector3 neutralParallel = (neutral_rho.Vect() * charged_rho.Vect().Unit()) * charged_rho.Vect().Unit();
    // Perpendicular component of pi0
    TVector3 neutralPerp = (neutral_rho.Vect() - neutralParallel).Unit();

    // the T-odd triple correlation (\mathcal(O)^{*}_{CP}) depends on the charge
    float triplecorr = track.Vect().Dot(neutralPerp.Cross(ipVectorPerp_track));
    if (track_is_plus) triplecorr = charged_rho.Vect().Dot(ipVectorPerp_track.Cross(neutralPerp));

    float phistar = TMath::ACos(neutralPerp.Dot(ipVectorPerp_track));
    if (triplecorr < 0) phistar = 2 * TMath::Pi() - phistar;

    return phistar;
}


float Acoplanarity_IP_rho_lepton(TLorentzVector track, TLorentzVector ipVector_track, TLorentzVector charged_rho,
                                 TLorentzVector neutral_rho,
                                 const TLorentzVector &referenceFrame, bool track_is_plus) {
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    track.Boost(boostIntoReferenceFrame);
    ipVector_track.Boost(boostIntoReferenceFrame);
    charged_rho.Boost(boostIntoReferenceFrame);
    neutral_rho.Boost(boostIntoReferenceFrame);

    // IP vector
    TVector3 ip3Vector_track = ipVector_track.Vect();

    // parallel projection of impact vector onto track direction
    TVector3 ipVectorParallel_track = (ip3Vector_track * track.Vect().Unit()) * track.Vect().Unit();
    // Perpendicular component of impact vector
    TVector3 ipVectorPerp_track = (ip3Vector_track - ipVectorParallel_track).Unit();

    // Parallel projections of pi0 onto pi+/- direction
    TVector3 neutralParallel = (neutral_rho.Vect() * charged_rho.Vect().Unit()) * charged_rho.Vect().Unit();
    // Perpendicular component of pi0
    TVector3 neutralPerp = (neutral_rho.Vect() - neutralParallel).Unit();

    // the T-odd triple correlation (\mathcal(O)^{*}_{CP}) depends on the charge
    float triplecorr = track.Vect().Dot(neutralPerp.Cross(ipVectorPerp_track));
    if (track_is_plus) triplecorr = charged_rho.Vect().Dot(ipVectorPerp_track.Cross(neutralPerp));

    float phistar = TMath::ACos(neutralPerp.Dot(ipVectorPerp_track));
    if (triplecorr < 0) phistar = 2 * TMath::Pi() - phistar;

    //a1_lep cancelled
//    if (phistar < TMath::Pi()){
//        phistar += TMath::Pi();
//    }
//
//    else{
//        phistar -= TMath::Pi();
//    }



    return phistar;
}

float Cal_1p1p_Upsilon(TLorentzVector charged, TLorentzVector neutral, const TLorentzVector &referenceFrame) {
    // only boost if we can boost into the reference frame
    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        charged.Boost(boostIntoReferenceFrame);
        neutral.Boost(boostIntoReferenceFrame);
    }
    return (charged.E() - neutral.E()) / (charged.E() + neutral.E());
}

float Cal_xTauFW_Upsilon_a1(TLorentzVector pion, TLorentzVector intermediate_rho) {
    float a1mass2 = (intermediate_rho + pion).M2();
    float y_a1 = (intermediate_rho.E() - pion.E()) / (intermediate_rho.E() + pion.E()) -
                 (a1mass2 - pion.M2() + intermediate_rho.M2()) / (2 * a1mass2);

    return y_a1;

}

float Cal_xTauFW_Upsilon_a1_with_ref(TLorentzVector pion, TLorentzVector intermediate_rho, const TLorentzVector &referenceFrame) {

    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        intermediate_rho.Boost(boostIntoReferenceFrame);
        pion.Boost(boostIntoReferenceFrame);
    }

    float a1mass2 = (intermediate_rho + pion).M2();
    float y_a1 = (intermediate_rho.E() - pion.E()) / (intermediate_rho.E() + pion.E()) -
                 (a1mass2 - pion.M2() + intermediate_rho.M2()) / (2 * a1mass2);

    return y_a1;

}

float Cal_3p_rho_Upsilon(TLorentzVector positive, TLorentzVector negative, const TLorentzVector &referenceFrame) {
    // only boost if we can boost into the reference frame
    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        positive.Boost(boostIntoReferenceFrame);
        negative.Boost(boostIntoReferenceFrame);
    }
    return (positive.E() - negative.E()) / (positive.E() + negative.E());
}



vector<float>
Boost_to_reference_Frame(const TLorentzVector Object_tobe_boosted, const TVector3 boostIntoReferenceFrame) {
    vector<float> E_PxPyPz;
    TLorentzVector Boosted_TLV;
    Boosted_TLV = Object_tobe_boosted;
    Boosted_TLV.Boost(boostIntoReferenceFrame);
    E_PxPyPz.push_back((Boosted_TLV.E()) / 10);
    E_PxPyPz.push_back((Boosted_TLV.Px()) / 5);
    E_PxPyPz.push_back((Boosted_TLV.Py()) / 5);
    E_PxPyPz.push_back((Boosted_TLV.Pz()) / 10);


    return E_PxPyPz;

}

TLorentzVector Make_ip_TLV(const Float_t ip_x, const Float_t ip_y, const Float_t ip_z) {

    TVector3 IP_V3;
    TVector3 IP_V3_Norm;
    IP_V3.SetXYZ(ip_x, ip_y, ip_z);
    IP_V3_Norm = IP_V3.Unit();


    TLorentzVector IP_TLV;
    IP_TLV.SetPxPyPzE(IP_V3_Norm.X(), IP_V3_Norm.Y(), IP_V3_Norm.Z(), 0);

    return IP_TLV;


}

void
Add_TLV_to_Vector(vector<float> &Data_collecting, const TLorentzVector TLV, const TVector3 boostIntoReferenceFrame) {

    vector<float> TLV_Boosted = Boost_to_reference_Frame(TLV, boostIntoReferenceFrame);
    Data_collecting.insert(Data_collecting.end(), TLV_Boosted.begin(), TLV_Boosted.end());
    return;

}


void Create_Pytorch_Input(vector <vector<float>> Dataset_Input, string file_name) {


    list<float> output_list;


    for (const auto &datum:Dataset_Input) {
        output_list.insert(output_list.end(), datum.begin(), datum.end());
    }


    auto json = TBufferJSON::ToJSON(&output_list);
    ofstream fout(file_name);
    fout << json;
    fout.close();
    return;
}

vector <vector<float>>
Create_shuffle_dataset(vector <vector<float>> Event_container, int num_events_in_input, int num_of_generated_inputs) {


    vector <vector<float>> Input_raw_vector = Event_container;
    vector <vector<float>> Output_vector;
    Output_vector.clear();

    for (int i_generated = 0; i_generated < num_of_generated_inputs; i_generated++) {

        random_shuffle(Input_raw_vector.begin(), Input_raw_vector.end());
        vector < vector < float >> ::const_iterator
        first_selection = Input_raw_vector.begin();
        vector < vector < float >> ::const_iterator
        last_selection = Input_raw_vector.begin() + num_events_in_input;
        vector <vector<float>> cut_vector_to_input(first_selection, last_selection);

        Output_vector.insert(Output_vector.end(), cut_vector_to_input.begin(), cut_vector_to_input.end());

    }


    return Output_vector;


}


void Create_training_dataset(vector<vector<float>> Event_container, vector<vector<float>> &Training_inputs_container,
                             vector<vector<float>> &Training_labels_container, int num_events_in_input,
                             int num_of_generated_inputs) {


    vector <vector<float>> Input_raw_vector = Event_container;


    for (int i_generated = 0; i_generated < num_of_generated_inputs; i_generated++) {

        if(i_generated%100 ==0){
            cout<<"current progress = "<< i_generated<<endl;

        }

        random_shuffle(Input_raw_vector.begin(), Input_raw_vector.end());
        vector < vector < float >> ::const_iterator
        first_selection = Input_raw_vector.begin();
        vector < vector < float >> ::const_iterator
        last_selection = Input_raw_vector.begin() + num_events_in_input;
        vector <vector<float>> cut_vector_to_input(first_selection, last_selection);

        for(int i_event_num = 0; i_event_num<num_events_in_input; i_event_num++){
            vector<float>::const_iterator label_start  = cut_vector_to_input[i_event_num].begin();
            vector<float>::const_iterator label_end  = cut_vector_to_input[i_event_num].begin()+2;
            vector<float>::const_iterator data_end  = cut_vector_to_input[i_event_num].end();

            vector<float> cut_label(label_start, label_end);
            vector<float> cut_input(label_end, data_end);

//            cout<<"size = ( "<<cut_label.size()<<" , "<<cut_input.size()<<" )"<<endl;

            Training_labels_container.push_back(cut_label);
            Training_inputs_container.push_back(cut_input);



        }



    }



    return;


}



void Print_training_dataset_Sig_BK(vector <vector<float>> Signal_Event_container,
                                   vector <vector<float>> Background_Event_container,
                                   vector <vector<float>> &Training_inputs_container,
                                   vector <vector<float>> &Training_labels_container, int num_sig_events_in_input,
                                   int num_bk_events_in_input,
                                   int num_of_generated_inputs) {


    vector <vector<float>> Input_raw_vector_sig = Signal_Event_container;
    vector <vector<float>> Input_raw_vector_bk = Background_Event_container;

    for (int i_generated = 0; i_generated < num_of_generated_inputs; i_generated++) {

        if(i_generated%50 ==0){
            cout<<"current progress = "<< i_generated<<endl;

        }

        random_shuffle(Input_raw_vector_sig.begin(), Input_raw_vector_sig.end());
        vector < vector < float >> ::const_iterator
        first_selection_sig = Input_raw_vector_sig.begin();
        vector < vector < float >> ::const_iterator
        last_selection_sig = Input_raw_vector_sig.begin() + num_sig_events_in_input;
        vector <vector<float>> cut_vector_to_input_sig(first_selection_sig, last_selection_sig);


        random_shuffle(Input_raw_vector_bk.begin(), Input_raw_vector_bk.end());
        vector < vector < float >> ::const_iterator
        first_selection_bk = Input_raw_vector_bk.begin();
        vector < vector < float >> ::const_iterator
        last_selection_bk = Input_raw_vector_bk.begin() + num_bk_events_in_input;
        vector <vector<float>> cut_vector_to_input_bk(first_selection_bk, last_selection_bk);

//        cout<<"Sig_data_size = "<<cut_vector_to_input_sig[0].size()<<"   , BK_data_size = "<<cut_vector_to_input_bk[0].size()<<endl;

        vector <vector<float>> cut_vector_to_input;
        cut_vector_to_input = cut_vector_to_input_sig;
        cut_vector_to_input.insert(cut_vector_to_input.end(), cut_vector_to_input_bk.begin(), cut_vector_to_input_bk.end());
        int total_events_of_sig_and_bk = num_sig_events_in_input + num_bk_events_in_input;
        for (int i_event_num = 0; i_event_num < total_events_of_sig_and_bk; i_event_num++) {
            vector<float>::const_iterator label_start = cut_vector_to_input[i_event_num].begin();
            vector<float>::const_iterator label_end = cut_vector_to_input[i_event_num].begin() + 2;
            vector<float>::const_iterator data_end = cut_vector_to_input[i_event_num].end();

            vector<float> cut_label(label_start, label_end);
            vector<float> cut_input(label_end, data_end);

//            cout<<"size = ( "<<cut_label.size()<<" , "<<cut_input.size()<<" )"<<endl;

            Training_labels_container.push_back(cut_label);
            Training_inputs_container.push_back(cut_input);


        }


    }


    return;


}



float Acoplanarity_RhoRho(TLorentzVector chargedPlus, TLorentzVector neutralPlus, TLorentzVector chargedMinus,
                                TLorentzVector neutralMinus, const TLorentzVector& referenceFrame) {
    // check that we do have valid charged and neutral 4 vectors
    //  if (chargedPlus.Mag() == 0 or neutralPlus.Mag() == 0 or chargedMinus.Mag() == 0 or neutralMinus.Mag() == 0) {
    //    return -5.;
    //  }
    // boost all 4-vectors into the reference frame
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    chargedPlus.Boost(boostIntoReferenceFrame);
    neutralPlus.Boost(boostIntoReferenceFrame);
    chargedMinus.Boost(boostIntoReferenceFrame);
    neutralMinus.Boost(boostIntoReferenceFrame);

    // parallel projections of pi0 onto pi+/- direction
    TVector3 neutralParallelPlus = (neutralPlus.Vect() * chargedPlus.Vect().Unit()) * chargedPlus.Vect().Unit();
    TVector3 neutralParallelMinus = (neutralMinus.Vect() * chargedMinus.Vect().Unit()) * chargedMinus.Vect().Unit();

    // perpendicular component of pi0
    TVector3 neutralPerpPlus = (neutralPlus.Vect() - neutralParallelPlus).Unit();
    TVector3 neutralPerpMinus = (neutralMinus.Vect() - neutralParallelMinus).Unit();

    float triplecorr =
        chargedMinus.Vect().Dot(neutralPerpPlus.Cross(neutralPerpMinus));  // the T-odd triple correlation \mathcal(O)^{*}_{CP}

    float phistar = TMath::ACos(neutralPerpPlus.Dot(neutralPerpMinus));
    if (triplecorr < 0) phistar = 2 * TMath::Pi() - phistar;

    return phistar;

    // Do upsilon separation in xVarGroups.cxx
}

vector<float> Cal_Observable_xTauFW_NC_LpT(const TLorentzVector tau_0_T0_p4, const TLorentzVector tau_0_T1_p4, const TLorentzVector tau_0_T2_p4,const TLorentzVector tau_1_charged_p4, const TLorentzVector tau_1_neutral_p4, float tau0_charge, float tau0_T0_q, float tau0_T1_q, float tau0_T2_q, const TLorentzVector& referenceFrame){




       // the pion
    float rhomass = 0.775;
    float best_mass_diff = 9999;

    vector<float> Observable_xTauFW_NC_LpT;
    TLorentzVector intermediate_rho = tau_0_T1_p4 + tau_0_T2_p4;
    TLorentzVector pion = tau_0_T0_p4;
    TLorentzVector Charge_Plus;
    TLorentzVector Neutral_Plus;
    TLorentzVector Charge_Minus;
    TLorentzVector Neutral_Minus;
    float Tau_1_upsilon = Cal_1p1p_Upsilon(tau_1_charged_p4,tau_1_neutral_p4,referenceFrame);
    if(tau0_charge>0){
        Charge_Plus = pion;
        Neutral_Plus = intermediate_rho;
        Charge_Minus = tau_1_charged_p4;
        Neutral_Minus = tau_1_neutral_p4;
    }
    else{
        Charge_Minus = pion;
        Neutral_Minus = intermediate_rho;
        Charge_Plus = tau_1_charged_p4;
        Neutral_Plus = tau_1_neutral_p4;
    }
    float phistar = Acoplanarity_RhoRho(Charge_Plus, Neutral_Plus, Charge_Minus, Neutral_Minus, referenceFrame);



    if (phistar < TMath::Pi())
        phistar += TMath::Pi();
    else
        phistar -= TMath::Pi();

    float phi_star_origin = phistar;
    float Upsilon_a1 = Cal_xTauFW_Upsilon_a1(pion,intermediate_rho);
    if (Upsilon_a1 * Tau_1_upsilon > 0) { /* OK */
    } else {
        // Add +/- pi
        if (phistar < TMath::Pi())
            phistar += TMath::Pi();
        else
            phistar -= TMath::Pi();
    }
    Observable_xTauFW_NC_LpT.push_back(phistar);
    Observable_xTauFW_NC_LpT.push_back(Upsilon_a1);

    float phi_star_inverse = phistar;

    if (phi_star_inverse < TMath::Pi())
        phi_star_inverse += TMath::Pi();
    else
        phi_star_inverse -= TMath::Pi();
    Observable_xTauFW_NC_LpT.push_back(phi_star_origin);
    Observable_xTauFW_NC_LpT.push_back(phi_star_inverse);
    return Observable_xTauFW_NC_LpT;
}





vector<float> Cal_Observable_Was_4_planes(const TLorentzVector tau_0_T0_p4, const TLorentzVector tau_0_T1_p4, const TLorentzVector tau_0_T2_p4,const TLorentzVector tau_1_charged_p4, const TLorentzVector tau_1_neutral_p4, float tau0_charge, float tau0_T0_q, float tau0_T1_q, float tau0_T2_q, const TLorentzVector& referenceFrame){


    vector<float> Observable_Was_4_planes;
    TLorentzVector intermediate_rho = tau_0_T1_p4 + tau_0_T2_p4;
    TLorentzVector pion = tau_0_T0_p4;
    TLorentzVector Charge_Plus_0;
    TLorentzVector Neutral_Plus_0;
    TLorentzVector Charge_Minus_0;
    TLorentzVector Neutral_Minus_0;

    TLorentzVector Charge_Plus_1;
    TLorentzVector Neutral_Plus_1;
    TLorentzVector Charge_Minus_1;
    TLorentzVector Neutral_Minus_1;

    float phistar_0;
    float phistar_0_in;
    float phistar_1;
    float phistar_1_in;

    TLorentzVector Oppo_charged_track;
    TLorentzVector Same_charged_track_0;
    TLorentzVector Same_charged_track_1;
    TLorentzVector Inter_rho_0;
    TLorentzVector Pion_0;
    TLorentzVector In_Track_0;
    TLorentzVector Inter_rho_1;
    TLorentzVector Pion_1;
    TLorentzVector In_Track_1;

    float Upsilon_a1_0;
    float Upsilon_a1_1;
    float Upsilon_oppo_0;
    float Upsilon_oppo_1;

    if(tau0_charge*tau0_T0_q <0){
        Oppo_charged_track = tau_0_T0_p4;
        Same_charged_track_0 = tau_0_T1_p4;
        Same_charged_track_1 = tau_0_T2_p4;
        Inter_rho_0 =tau_0_T0_p4+tau_0_T1_p4;
        Pion_0 = tau_0_T2_p4;
        In_Track_0 = tau_0_T1_p4;
        Inter_rho_1 =tau_0_T0_p4+tau_0_T2_p4;
        Pion_1 = tau_0_T1_p4;
        In_Track_1 = tau_0_T2_p4;


    }
    else if(tau0_charge*tau0_T1_q <0){
        Oppo_charged_track = tau_0_T1_p4;
        Same_charged_track_0 = tau_0_T0_p4;
        Same_charged_track_1 = tau_0_T2_p4;
        Inter_rho_0 =tau_0_T1_p4+tau_0_T0_p4;
        Pion_0 = tau_0_T2_p4;
        In_Track_0 = tau_0_T0_p4;
        Inter_rho_1 =tau_0_T1_p4+tau_0_T2_p4;
        Pion_1 = tau_0_T0_p4;
        In_Track_1 = tau_0_T2_p4;

    }
    else if(tau0_charge*tau0_T2_q <0){
        Oppo_charged_track = tau_0_T2_p4;
        Same_charged_track_0 = tau_0_T0_p4;
        Same_charged_track_1 = tau_0_T1_p4;
        Inter_rho_0 =tau_0_T2_p4+tau_0_T0_p4;
        Pion_0 = tau_0_T1_p4;
        In_Track_0 = tau_0_T0_p4;
        Inter_rho_1 =tau_0_T2_p4+tau_0_T1_p4;
        Pion_1 = tau_0_T0_p4;
        In_Track_1 = tau_0_T1_p4;
    }
    else{
        return Observable_Was_4_planes;
    }

    float Tau_1_upsilon = Cal_1p1p_Upsilon(tau_1_charged_p4,tau_1_neutral_p4,referenceFrame);
    if(tau0_charge>0){

        phistar_0 =Acoplanarity_RhoRho(Pion_0, Inter_rho_0, tau_1_charged_p4, tau_1_neutral_p4, referenceFrame);
        phistar_0_in = Acoplanarity_RhoRho(In_Track_0, Oppo_charged_track, tau_1_charged_p4, tau_1_neutral_p4, referenceFrame);
        phistar_1 = Acoplanarity_RhoRho(Pion_1, Inter_rho_1, tau_1_charged_p4, tau_1_neutral_p4, referenceFrame);
        phistar_1_in = Acoplanarity_RhoRho(In_Track_1, Oppo_charged_track, tau_1_charged_p4, tau_1_neutral_p4, referenceFrame);
        Upsilon_a1_0 = Cal_xTauFW_Upsilon_a1(Pion_0,Inter_rho_0);
        Upsilon_a1_1 = Cal_xTauFW_Upsilon_a1(Pion_1,Inter_rho_1);
        Upsilon_oppo_0 = Cal_3p_rho_Upsilon(In_Track_0,Oppo_charged_track, referenceFrame);
        Upsilon_oppo_1 = Cal_3p_rho_Upsilon(In_Track_1,Oppo_charged_track, referenceFrame);

    }
    else{

        phistar_0 =Acoplanarity_RhoRho(tau_1_charged_p4, tau_1_neutral_p4, Pion_0, Inter_rho_0,  referenceFrame);
        phistar_0_in = Acoplanarity_RhoRho(tau_1_charged_p4, tau_1_neutral_p4, In_Track_0, Oppo_charged_track,  referenceFrame);
        phistar_1 = Acoplanarity_RhoRho(tau_1_charged_p4, tau_1_neutral_p4, Pion_1, Inter_rho_1,  referenceFrame);
        phistar_1_in = Acoplanarity_RhoRho(tau_1_charged_p4, tau_1_neutral_p4, In_Track_1, Oppo_charged_track,  referenceFrame);
        Upsilon_a1_0 = Cal_xTauFW_Upsilon_a1(Pion_0,Inter_rho_0);
        Upsilon_a1_1 = Cal_xTauFW_Upsilon_a1(Pion_1,Inter_rho_1);
        Upsilon_oppo_0 = Cal_3p_rho_Upsilon(Oppo_charged_track,In_Track_0, referenceFrame);
        Upsilon_oppo_1 = Cal_3p_rho_Upsilon(Oppo_charged_track,In_Track_1, referenceFrame);
    }




    if (phistar_0 < TMath::Pi())
        phistar_0 += TMath::Pi();
    else
        phistar_0 -= TMath::Pi();

    if (phistar_0_in < TMath::Pi())
        phistar_0_in += TMath::Pi();
    else
        phistar_0_in -= TMath::Pi();

    if (phistar_1 < TMath::Pi())
        phistar_1 += TMath::Pi();
    else
        phistar_1 -= TMath::Pi();

    if (phistar_1_in < TMath::Pi())
        phistar_1_in += TMath::Pi();
    else
        phistar_1_in -= TMath::Pi();


    float phistar_0_ori =phistar_0;
    float phistar_0_in_ori =phistar_0_in;
    float phistar_1_ori = phistar_1;
    float phistar_1_in_ori =phistar_1_in;





    if (Upsilon_a1_0 * Tau_1_upsilon > 0) { /* OK */
    } else {
        // Add +/- pi
        if (phistar_0 < TMath::Pi())
            phistar_0 += TMath::Pi();
        else
            phistar_0 -= TMath::Pi();
    }


    if (Upsilon_oppo_0 * Tau_1_upsilon > 0) { /* OK */
    } else {
        // Add +/- pi
        if (phistar_0_in < TMath::Pi())
            phistar_0_in += TMath::Pi();
        else
            phistar_0_in -= TMath::Pi();
    }

    if (Upsilon_a1_1 * Tau_1_upsilon > 0) { /* OK */
    } else {
        // Add +/- pi
        if (phistar_1 < TMath::Pi())
            phistar_1 += TMath::Pi();
        else
            phistar_1 -= TMath::Pi();
    }

    if (Upsilon_oppo_1 * Tau_1_upsilon > 0) { /* OK */
    } else {
        // Add +/- pi
        if (phistar_1_in < TMath::Pi())
            phistar_1_in += TMath::Pi();
        else
            phistar_1_in -= TMath::Pi();
    }

    float phistar_0_inverse =phistar_0;
    float phistar_0_in_inverse =phistar_0_in;
    float phistar_1_inverse = phistar_1;
    float phistar_1_in_inverse =phistar_1_in;



    if (phistar_0_inverse < TMath::Pi())
        phistar_0_inverse += TMath::Pi();
    else
        phistar_0_inverse -= TMath::Pi();

    if (phistar_0_in_inverse < TMath::Pi())
        phistar_0_in_inverse += TMath::Pi();
    else
        phistar_0_in_inverse -= TMath::Pi();

    if (phistar_1_inverse < TMath::Pi())
        phistar_1_inverse += TMath::Pi();
    else
        phistar_1_inverse -= TMath::Pi();

    if (phistar_1_in_inverse < TMath::Pi())
        phistar_1_in_inverse += TMath::Pi();
    else
        phistar_1_in_inverse -= TMath::Pi();




    Observable_Was_4_planes.push_back(phistar_0);
    Observable_Was_4_planes.push_back(Upsilon_a1_0);
    Observable_Was_4_planes.push_back(phistar_0_inverse);
    Observable_Was_4_planes.push_back(phistar_0_ori);

    Observable_Was_4_planes.push_back(phistar_0_in);
    Observable_Was_4_planes.push_back(Upsilon_oppo_0);
    Observable_Was_4_planes.push_back(phistar_0_in_inverse);
    Observable_Was_4_planes.push_back(phistar_0_in_ori);

    Observable_Was_4_planes.push_back(phistar_1);
    Observable_Was_4_planes.push_back(Upsilon_a1_1);
    Observable_Was_4_planes.push_back(phistar_1_inverse);
    Observable_Was_4_planes.push_back(phistar_1_ori);


    Observable_Was_4_planes.push_back(phistar_1_in);
    Observable_Was_4_planes.push_back(Upsilon_oppo_1);
    Observable_Was_4_planes.push_back(phistar_1_in_inverse);
    Observable_Was_4_planes.push_back(phistar_1_in_ori);

    return Observable_Was_4_planes;
}

vector<float> Fill_p4_to_vector(const TLorentzVector Input_TLV){
    vector<float> Result_vector;
    Result_vector.clear();
    Result_vector.push_back(Input_TLV.E()/100);
    Result_vector.push_back(Input_TLV.Px()/100);
    Result_vector.push_back(Input_TLV.Py()/100);
    Result_vector.push_back(Input_TLV.Pz()/100);
    return Result_vector;
}

vector<vector<float>> Acoplanarity_A1Rho_All(const TLorentzVector tau_0_T0_p4, const TLorentzVector tau_0_T1_p4, const TLorentzVector tau_0_T2_p4,const TLorentzVector tau_1_charged_p4, const TLorentzVector tau_1_neutral_p4, float tau0_charge, float tau0_T0_q, float tau0_T1_q, float tau0_T2_q) {

    vector<vector<float>> All_Results;
    All_Results.clear();


    TLorentzVector referenceFrame = tau_0_T0_p4 + tau_0_T1_p4 + tau_0_T2_p4 + tau_1_charged_p4 + tau_1_neutral_p4;
    TLorentzVector Charge_Plus;
    TLorentzVector Neutral_Plus;
    TLorentzVector Charge_Minus;
    TLorentzVector Neutral_Minus;





    vector<float> Observable_xTauFW_NC_LpT = Cal_Observable_xTauFW_NC_LpT(tau_0_T0_p4, tau_0_T1_p4, tau_0_T2_p4,tau_1_charged_p4,tau_1_neutral_p4, tau0_charge,  tau0_T0_q, tau0_T1_q,tau0_T2_q, referenceFrame);
    All_Results.push_back(Observable_xTauFW_NC_LpT);

    vector<float> Observable_Was_4_planes = Cal_Observable_Was_4_planes(tau_0_T0_p4, tau_0_T1_p4, tau_0_T2_p4,tau_1_charged_p4,tau_1_neutral_p4, tau0_charge,  tau0_T0_q, tau0_T1_q,tau0_T2_q, referenceFrame);
    All_Results.push_back(Observable_Was_4_planes);


    All_Results.push_back(Fill_p4_to_vector(tau_0_T0_p4));
    All_Results.push_back(Fill_p4_to_vector(tau_0_T1_p4));
    All_Results.push_back(Fill_p4_to_vector(tau_0_T2_p4));
    All_Results.push_back(Fill_p4_to_vector(tau_1_charged_p4));
    All_Results.push_back(Fill_p4_to_vector(tau_1_neutral_p4));

    vector<float> Charge_info;
    Charge_info.push_back(tau0_charge);
    Charge_info.push_back(tau0_T0_q);
    Charge_info.push_back(tau0_T1_q);
    Charge_info.push_back(tau0_T2_q);

    All_Results.push_back(Charge_info);




    return All_Results;
}

int Preselection_Check(int Trigger_0, int Trigger_1, int Track_num_0, int Track_num_1, float Charge_0, float Charge_1,
                       float Charge_Product) {

    if ((Trigger_0 != 0 && (Track_num_0 == 1 || Track_num_0 == 3) && abs(Charge_0) == 1) &&
        (Trigger_1 != 0 && (Track_num_1 == 1 || Track_num_1 == 3) && abs(Charge_1) == 1) && Charge_Product < 0) {
        return 1;
    } else {
        return 0;
    }
}


float Check_2020_08_31_new_preselection(const TLorentzVector tau0, const TLorentzVector tau1, const TLorentzVector met,
                                        const TLorentzVector jet0, const TLorentzVector jet1, float MMC) {

    float jet0_pt = jet0.Pt();
    float jet1_pt = jet1.Pt();

    float tau0_pt = tau0.Pt();
    float tau1_pt = tau1.Pt();

    float jet1_eta = abs(jet1.Eta());

    float Delat_R_Taus = sqrt(
        (tau0.Eta() - tau1.Eta()) * (tau0.Eta() - tau1.Eta()) + (tau0.Phi() - tau1.Phi()) * (tau0.Phi() - tau1.Phi()));

    float Delta_eta_taus = sqrt((tau0.Phi() - tau1.Phi()) * (tau0.Phi() - tau1.Phi()));
    float ETMiss = met.Et();


    if ((jet0_pt > 70) && (tau0_pt > 40) && (tau1_pt > 30) && (jet1_eta < 3.2) && (Delat_R_Taus < 2.5) &&
        (Delat_R_Taus > 0.6) && (Delta_eta_taus < 1.5) && (ETMiss > 20) && (MMC > 110) && (MMC < 150)) {
        return 1;
    } else {
        return 0;
    }


}


void Convert_2D_Vector_to_txt(vector<vector<float>> Input_Vector, string Name_of_file){
    ofstream myfile (Name_of_file);
    if (myfile.is_open())
    {
        int input_size = Input_Vector.size();
        if(input_size>0){
            int variables_size = Input_Vector[0].size();

            for(int input_index =0; input_index<input_size;input_index++){
                for(int variable_index = 0 ; variable_index<variables_size; variable_index++){

                    myfile << Input_Vector[input_index][variable_index];
                    myfile << " ";
                }
                myfile << "\n";
            }
        }
        myfile.close();
    }
    else cout << "Unable to open file";
    return;

}



int Create_json() {

    TChain *chain_Calculate_MC_Sig = new TChain("Evaluated_TreeS;4");
    TString Final_Name = "./Plots/";



//    for (const auto &filename : glob(
//        "../../../../CERN_BOX_SYN/Test_New_Branches/Test_LH3P_Grid_0413/*/*.root")) {
//        chain_Calculate_MC_Sig->Add(filename.c_str());
//    }

    for (const auto &filename : glob(
        "../../../Pytorch_tau_decay_study/Read_Elzbieta_sample/ATLAS_type_output/ATLAS_Sample_Elz_a1rho_a.root")) {
        chain_Calculate_MC_Sig->Add(filename.c_str());
    }

    Double_t tauspinner_HCP_Theta_0;
    Double_t tauspinner_HCP_Theta_90;
    UInt_t tau_0_n_charged_tracks;
    UInt_t tau_1_n_charged_tracks;
    Float_t tau_0_track0_charge;
    Float_t tau_0_track1_charge;
    Float_t tau_0_track2_charge;
    TLorentzVector *tau_0_track0_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track1_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track2_p4 = new TLorentzVector;
    TLorentzVector *ditau_p4 = new TLorentzVector;
    TLorentzVector *tau_1_decay_charged_p4 = new TLorentzVector;
    TLorentzVector *tau_1_decay_neutral_p4 = new TLorentzVector;
    UInt_t tau_0_decay_mode;
    UInt_t tau_1_decay_mode;
    Float_t ditau_qxq;
    Float_t tau_0_q;
    Float_t tau_1_q;
    TLorentzVector *tau_0_neutrino_p4 = new TLorentzVector;
    TLorentzVector *tau_1_neutrino_p4 = new TLorentzVector;


    chain_Calculate_MC_Sig->SetBranchAddress("tauspinner_HCP_Theta_0", &tauspinner_HCP_Theta_0);
    chain_Calculate_MC_Sig->SetBranchAddress("tauspinner_HCP_Theta_90", &tauspinner_HCP_Theta_90);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_0_q", &tau_0_q);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_1_q", &tau_1_q);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_0_track0_p4", &tau_0_track0_p4);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_0_track1_p4", &tau_0_track1_p4);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_0_track2_p4", &tau_0_track2_p4);
    chain_Calculate_MC_Sig->SetBranchAddress("ditau_p4", &ditau_p4);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_1_decay_charged_p4", &tau_1_decay_charged_p4);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_1_decay_neutral_p4", &tau_1_decay_neutral_p4);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_0_track0_charge", &tau_0_track0_charge);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_0_track1_charge", &tau_0_track1_charge);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_0_track2_charge", &tau_0_track2_charge);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_0_neutrino_p4", &tau_0_neutrino_p4);
    chain_Calculate_MC_Sig->SetBranchAddress("tau_1_neutrino_p4", &tau_1_neutrino_p4);

    chain_Calculate_MC_Sig->Branch("tau_0_decay_mode", &tau_0_decay_mode);
    chain_Calculate_MC_Sig->Branch("tau_1_decay_mode", &tau_1_decay_mode);
    chain_Calculate_MC_Sig->Branch("ditau_qxq", &ditau_qxq);
    chain_Calculate_MC_Sig->Branch("tau_0_n_charged_tracks", &tau_0_n_charged_tracks);
    chain_Calculate_MC_Sig->Branch("tau_1_n_charged_tracks", &tau_1_n_charged_tracks);

    vector <vector<float>> Sample_3p1p_Dataset;
    vector <vector<float>> Events_Collection_Sig;
    vector <vector<float>> Training_data_Collection;
    vector <vector<float>> Training_label_Collection;
    Training_data_Collection.clear();
    Training_label_Collection.clear();

    int Num_of_3p1p = 0;



    Int_t nentries_sig = (Int_t) chain_Calculate_MC_Sig->GetEntries();
    for (Int_t i = 0; i < nentries_sig; i++) {
        chain_Calculate_MC_Sig->GetEntry(i);
        vector<float> A_single_Event;
        A_single_Event.clear();
//        cout<<"Check 0"<<endl;
//        cout<<"tau_0_track0_charge = "<<tau_0_track0_charge<<endl;
//        cout<<"tau_0_track1_charge = "<<tau_0_track1_charge<<endl;
//        cout<<"tauspinner_HCP_Theta_0 = "<<tauspinner_HCP_Theta_0<<endl;

//        if (tau_0_decay_mode == 3 && tau_1_decay_mode == 1){
////            cout<<"Check 1"<<endl;
//        }
//        else{
////            cout<<"Check 2"<<endl;
//            continue;
//        }

        if (1>0) {

            //Labels
            A_single_Event.push_back(float(tauspinner_HCP_Theta_0));
            A_single_Event.push_back(float(tauspinner_HCP_Theta_90));
            //Inputs
//
//            A_single_Event.push_back(float(ditau_CP_phi_star_cp_a1_rho));
            Num_of_3p1p += 1;
//            Events_Collection_Sig.push_back(A_single_Event);
//            cout<<A_single_Event.size();

            vector<vector<float>> Test_All_phi_star = Acoplanarity_A1Rho_All(*tau_0_track0_p4,*tau_0_track1_p4,*tau_0_track2_p4,*tau_1_decay_charged_p4,*tau_1_decay_neutral_p4,tau_0_q,tau_0_track0_charge,tau_0_track1_charge,tau_0_track2_charge);

            int Entries_of_Test_all = Test_All_phi_star.size();

            for(int i_vectors = 0; i_vectors<Entries_of_Test_all; i_vectors++){
                A_single_Event.insert(A_single_Event.end(),Test_All_phi_star[i_vectors].begin(),Test_All_phi_star[i_vectors].end());

            }







//            cout<<"Phistars = [ "<<phi_star_xTauFW<<" , "<<phi_star_Was_0<<" , "<<phi_star_Was_0_in<<" , "<<phi_star_Was_1<<" , "<<phi_star_Was_1_in<<" ] "<<endl;






            Events_Collection_Sig.push_back(A_single_Event);
//            cout<<A_single_Event.size()<<endl;





        }


    }




    vector <vector<float>> Training_data_Collection_Sig_BK;
    vector <vector<float>> Training_label_Collection_Sig_BK;
    Training_data_Collection_Sig_BK.clear();
    Training_label_Collection_Sig_BK.clear();


    TChain *chain_Calculate_MC_BK = new TChain("Evaluated_TreeS;4");


    for (const auto &filename : glob(
        "./ATLAS_Form_Elz/ATLAS_type_Elz_a1rho_Ztt_seed0.root")) {
        chain_Calculate_MC_BK->Add(filename.c_str());
    }

    Double_t tauspinner_HCP_Theta_0_BK;
    Double_t tauspinner_HCP_Theta_90_BK;
    UInt_t tau_0_n_charged_tracks_BK;
    UInt_t tau_1_n_charged_tracks_BK;
    Float_t tau_0_track0_charge_BK;
    Float_t tau_0_track1_charge_BK;
    Float_t tau_0_track2_charge_BK;
    TLorentzVector *tau_0_track0_p4_BK = new TLorentzVector;
    TLorentzVector *tau_0_track1_p4_BK = new TLorentzVector;
    TLorentzVector *tau_0_track2_p4_BK = new TLorentzVector;
    TLorentzVector *ditau_p4_BK = new TLorentzVector;
    TLorentzVector *tau_1_decay_charged_p4_BK = new TLorentzVector;
    TLorentzVector *tau_1_decay_neutral_p4_BK = new TLorentzVector;
    UInt_t tau_0_decay_mode_BK;
    UInt_t tau_1_decay_mode_BK;
    Float_t ditau_qxq_BK;
    Float_t tau_0_q_BK;
    Float_t tau_1_q_BK;
    TLorentzVector *tau_0_neutrino_p4_BK = new TLorentzVector;
    TLorentzVector *tau_1_neutrino_p4_BK = new TLorentzVector;


    chain_Calculate_MC_BK->SetBranchAddress("tau_0_q", &tau_0_q_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_1_q", &tau_1_q_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_0_track0_p4", &tau_0_track0_p4_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_0_track1_p4", &tau_0_track1_p4_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_0_track2_p4", &tau_0_track2_p4_BK);
    chain_Calculate_MC_BK->SetBranchAddress("ditau_p4", &ditau_p4_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_1_decay_charged_p4", &tau_1_decay_charged_p4_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_1_decay_neutral_p4", &tau_1_decay_neutral_p4_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_0_track0_charge", &tau_0_track0_charge_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_0_track1_charge", &tau_0_track1_charge_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_0_track2_charge", &tau_0_track2_charge_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_0_neutrino_p4", &tau_0_neutrino_p4_BK);
    chain_Calculate_MC_BK->SetBranchAddress("tau_1_neutrino_p4", &tau_1_neutrino_p4_BK);

    chain_Calculate_MC_BK->Branch("tau_0_decay_mode", &tau_0_decay_mode_BK);
    chain_Calculate_MC_BK->Branch("tau_1_decay_mode", &tau_1_decay_mode_BK);
    chain_Calculate_MC_BK->Branch("ditau_qxq", &ditau_qxq_BK);
    chain_Calculate_MC_BK->Branch("tau_0_n_charged_tracks", &tau_0_n_charged_tracks_BK);
    chain_Calculate_MC_BK->Branch("tau_1_n_charged_tracks", &tau_1_n_charged_tracks_BK);

    vector <vector<float>> Sample_3p1p_Dataset_BK;
    vector <vector<float>> Events_Collection_BK;
    vector <vector<float>> Training_data_Collection_BK;
    vector <vector<float>> Training_label_Collection_BK;
    Training_data_Collection_BK.clear();
    Training_label_Collection_BK.clear();


    tauspinner_HCP_Theta_0_BK = float(1.0);
    tauspinner_HCP_Theta_90_BK = float(1.0);

    Int_t nentries_bk = (Int_t) chain_Calculate_MC_BK->GetEntries();
    for (Int_t i = 0; i < nentries_bk; i++) {
        chain_Calculate_MC_BK->GetEntry(i);
        vector<float> A_single_Event;
        A_single_Event.clear();
//        cout<<"Check 0"<<endl;
//        cout<<"tau_0_track0_charge = "<<tau_0_track0_charge<<endl;
//        cout<<"tau_0_track1_charge = "<<tau_0_track1_charge<<endl;
//        cout<<"tauspinner_HCP_Theta_0 = "<<tauspinner_HCP_Theta_0<<endl;

//        if (tau_0_decay_mode == 3 && tau_1_decay_mode == 1){
////            cout<<"Check 1"<<endl;
//        }
//        else{
////            cout<<"Check 2"<<endl;
//            continue;
//        }

        if (1>0) {

            //Labels
            A_single_Event.push_back(float(tauspinner_HCP_Theta_0_BK));
            A_single_Event.push_back(float(tauspinner_HCP_Theta_90_BK));
            //Inputs
//
//            A_single_Event.push_back(float(ditau_CP_phi_star_cp_a1_rho));
            Num_of_3p1p += 1;
//            Events_Collection_Sig.push_back(A_single_Event);
//            cout<<A_single_Event.size();

            vector<vector<float>> Test_All_phi_star = Acoplanarity_A1Rho_All(*tau_0_track0_p4_BK,*tau_0_track1_p4_BK,*tau_0_track2_p4_BK,*tau_1_decay_charged_p4_BK,*tau_1_decay_neutral_p4_BK,tau_0_q_BK,tau_0_track0_charge_BK,tau_0_track1_charge_BK,tau_0_track2_charge_BK);

            int Entries_of_Test_all = Test_All_phi_star.size();

            for(int i_vectors = 0; i_vectors<Entries_of_Test_all; i_vectors++){
                A_single_Event.insert(A_single_Event.end(),Test_All_phi_star[i_vectors].begin(),Test_All_phi_star[i_vectors].end());

            }








//            cout<<"Phistars = [ "<<phi_star_xTauFW<<" , "<<phi_star_Was_0<<" , "<<phi_star_Was_0_in<<" , "<<phi_star_Was_1<<" , "<<phi_star_Was_1_in<<" ] "<<endl;






            Events_Collection_BK.push_back(A_single_Event);
//            cout<<A_single_Event.size()<<endl;





        }


    }



    int Entries_of_dataset = Events_Collection_Sig.size();
    cout<<"Event_Collection size = "<<Entries_of_dataset<<endl;
    cout<<"=========Check_Max_Min_Mean================"<<endl;
    vector<float> lowest = Events_Collection_Sig[0];
    vector<float> highest = Events_Collection_Sig[0];
    int number_in_each_data = Events_Collection_Sig[0].size();
    for(int i_vectors_data = 0; i_vectors_data<Entries_of_dataset; i_vectors_data++){

        for(int i_inputs_number = 0; i_inputs_number<number_in_each_data; i_inputs_number++){

            if(Events_Collection_Sig[i_vectors_data][i_inputs_number]<lowest[i_inputs_number]){
                lowest[i_inputs_number] = Events_Collection_Sig[i_vectors_data][i_inputs_number];

            }
            if(Events_Collection_Sig[i_vectors_data][i_inputs_number]>highest[i_inputs_number]){
                highest[i_inputs_number] = Events_Collection_Sig[i_vectors_data][i_inputs_number];

            }

        }

    }
    for(int i_check = 0; i_check<number_in_each_data; i_check++){

        cout<<"index[ "<<i_check<<" ]   Lowest = "<<lowest[i_check]<<" ,  Highest = "<<highest[i_check]<<endl;

    }
    cout<<"====================================="<<endl;


    int Entries_of_dataset_BK = Events_Collection_BK.size();
    cout << "Event_Collection size = " << Entries_of_dataset_BK << endl;
    cout << "=========Check_Max_Min_Mean_BK================" << endl;
    vector<float> lowest_BK = Events_Collection_BK[0];
    vector<float> highest_BK = Events_Collection_BK[0];
    int number_in_each_data_BK = Events_Collection_BK[0].size();
    for (int i_vectors_data = 0; i_vectors_data < Entries_of_dataset_BK; i_vectors_data++) {

        for (int i_inputs_number = 0; i_inputs_number < number_in_each_data_BK; i_inputs_number++) {

            if (Events_Collection_BK[i_vectors_data][i_inputs_number] < lowest_BK[i_inputs_number]) {
                lowest_BK[i_inputs_number] = Events_Collection_BK[i_vectors_data][i_inputs_number];

            }
            if (Events_Collection_BK[i_vectors_data][i_inputs_number] > highest_BK[i_inputs_number]) {
                highest_BK[i_inputs_number] = Events_Collection_BK[i_vectors_data][i_inputs_number];

            }

        }

    }
    for (int i_check = 0; i_check < number_in_each_data_BK; i_check++) {

        cout << "index[ " << i_check << " ]   Lowest = " << lowest_BK[i_check] << " ,  Highest = "
             << highest_BK[i_check] << endl;

    }
    cout << "=====================================" << endl;







//
//

//
//    Sample_3p1p_Dataset = Create_shuffle_dataset(Events_Collection_Sig, Num_of_Events_in_an_input, Number_of_inputs);
//    Create_training_dataset(Events_Collection_Sig, Training_data_Collection, Training_label_Collection,Num_of_Events_in_an_input, Number_of_inputs);
    Print_training_dataset_Sig_BK(Events_Collection_Sig,Events_Collection_BK,Training_data_Collection_Sig_BK,Training_label_Collection_Sig_BK,Number_of_signal_inputs,Number_of_background_inputs,Number_of_inputs);

////
//    cout << "Num of 3p1p = " << Num_of_3p1p << endl;
//
//
////
//
//    cout<<"size = ["<<Training_data_Collection_Sig_BK.size()<<"]["<<Training_data_Collection_Sig_BK[0].size()<<"]"<<endl;

    Create_Pytorch_Input(Training_data_Collection_Sig_BK,"./NN_Samples/Test_NN_Inputs_Elz_S_with_B_080601.json");
    Create_Pytorch_Input(Training_label_Collection_Sig_BK,"./NN_Samples/Test_NN_Labels_Elz_S_with_B_080601.json");

//
//
//
//    auto json = TBufferJSON::ToJSON(&output_list_phi_star);
//    ofstream fout("ntuple_sample_phi_star_3p1n_test.json");
//    fout<<json;
//    fout.close();
//


    return 0;

}


void Collect_phi_star_info_Elz_with_BK() {


//    Make_Plots_for_d0();
//    Make_Plots_for_z0();
//    Make_Plots_for_distance();
//    Make_Plots_for_z0_diff();
//    Make_Plots_for_1p3p_rho_ip();
//    Check_Reco_Decay_vtx_exist();

    Create_json();


    return;
}

