
import numpy as np
import json
import ROOT
from ROOT import TFile, TTree, gRandom
from array import array



Start_Reading = 0
Particle_Index = 0


path_input = "../../Elzbieta_Sample/a1rho/Original_ASCII/"
file_name = "pythia.Z_115_135.a1rho.1M.b.outTUPLE_labFrame"
file_input = path_input + file_name

f = open(file_input)
# f = open("last_400.log")
line_reading = f.readline()
# print(line[0:5])


file_create = TFile("Elz_a1rho_Ztt_b.root", 'recreate')


tree = TTree("Elz_Sample", "Elz_Sample")
tauspinner = array('f', [0.])
tree.Branch("tauspinner",  tauspinner,  'tauspinner/F')


Particle_Px = ROOT.std.vector('float')()
Particle_Py = ROOT.std.vector('float')()
Particle_Pz = ROOT.std.vector('float')()
Particle_E = ROOT.std.vector('float')()
Particle_Id = ROOT.std.vector('int')()

tree.Branch("Particle_Px",  Particle_Px)
tree.Branch("Particle_Py",  Particle_Py)
tree.Branch("Particle_Pz",  Particle_Pz)
tree.Branch("Particle_E",  Particle_E)
tree.Branch("Particle_Id",  Particle_Id)





while line_reading:
    # print(line_reading, end = '')
    if(line_reading[0:5] == "Analy"):
        # print("=====Finish=====")
        Start_Reading = 0
    if (line_reading[0:5] == "TUPLE"):
        Start_Reading = 1
        # print("=====Tuple=====")
        # print(float(line_reading[5:]))
        tauspinner[0] = float(line_reading[5:])
        # print(tauspinner)
        Particle_Index = 0
    elif(Start_Reading != 0):
        num_array = line_reading.split()
        # print(num_array)
        Particle_Px.push_back(float(num_array[0]))
        Particle_Py.push_back(float(num_array[1]))
        Particle_Pz.push_back(float(num_array[2]))
        Particle_E.push_back(float(num_array[3]))
        Particle_Id.push_back(int(num_array[4]))
        Particle_Index +=1
        if(Particle_Index==7):
            tree.Fill()
            Particle_Px.clear()
            Particle_Py.clear()
            Particle_Pz.clear()
            Particle_E.clear()
            Particle_Id.clear()
        # print(num_array)
    elif (Start_Reading == 0):
        print(line_reading)


    line_reading = f.readline()

file_create.Write()
file_create.Close()